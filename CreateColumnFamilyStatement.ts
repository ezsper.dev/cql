import { BaseStatement } from './BaseStatement';
import { SelectStatement } from './SelectStatement';
import { InsertStatement } from './InsertStatement';
import { UpdateStatement } from './UpdateStatement';
import { DeleteStatement } from './DeleteStatement';
import { CreateMaterializedViewStatement } from './CreateMaterializedViewStatement';
import { CreateColumnFamilyIndexStatement } from './CreateColumnFamilyIndexStatement';
import { buildOptions } from './CreateKeyspaceStatement';
import { Raw } from './Raw';
import {
  ColumnDefinition,
  TransformColumnDefinition,
  TransformPossibleColumnDefinition,
  Selection,
  StringKey,
  getColumnType,
} from './ColumnDefinition';

export interface ColumnFamilyOptions {
  bloomFilterFpChance?: number;
  comment?: string;
  crcCheckChance?: number;
  dclocalReadRepairChange?: number;
  defaultTimeToLive?: number;
  gcGraceSeconds?: number;
  maxIndexInterval?: number;
  memtableFlushPeriodInMs?: number;
  minIndexInterval?: number;
  readRepairChance?: number;
  speculativeRetry?: string;
  caching?: {
    keys?: string;
    rowsPerPartition?: string;
  };
  compression?: {
    chunkLengthInKb?: number;
    class?: string;
    enabled?: boolean;
  };
  compaction?: {
    class?: string;
    maxThreshold?: number;
    minThreshold?: number;
  };
  [name: string]: any;
}

export class CreateColumnFamilyStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, I extends StringKey<R> = never, J extends Exclude<StringKey<R>, I> = never, N extends string = never> extends BaseStatement {

  static Update = UpdateStatement;
  static Insert = InsertStatement;
  static Delete = DeleteStatement;
  static CreateColumnFamilyIndex = CreateColumnFamilyIndexStatement;
  static CreateMaterializedView = CreateMaterializedViewStatement;

  protected nullableColumns: N[];
  protected keyspaceDef: Raw;
  protected ifNotExistsDef: boolean;
  protected columnFamilyDef: Raw;
  protected columnDefs: ColumnDefinition;
  protected partitionKeyDefs: I[] = [];
  protected clusteringKeyDefs: J[] = [];
  protected clusteringOrderDefs: ([J, 'ASC' | 'DESC'])[] = [];
  protected optionsDef: ColumnFamilyOptions;

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  as(name: string | Raw): this;
  as(keyspace: string | Raw, name: string | Raw): this;
  as(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  ifNotExists(value = true) {
    this.ifNotExistsDef = value;
    return this;
  }

  columns<B extends ColumnDefinition>(columns: B): CreateColumnFamilyStatement<TransformColumnDefinition<B>, TransformPossibleColumnDefinition<B>> {
    this.columnDefs = Object.assign({}, this.columnDefs, columns);
    return <any>this;
  }

  partitionKeys<B extends StringKey<R>>(...columns: B[]): CreateColumnFamilyStatement<R, S, B, never, N> {
    this.partitionKeyDefs = <any>columns;
    return <any>this;
  }

  clusteringKeys<B extends Exclude<StringKey<R>, I>>(...columns: B[]): CreateColumnFamilyStatement<R, S, I, B, N> {
    this.clusteringKeyDefs = <any>columns;
    return <any>this;
  }

  nullable<B extends StringKey<R>>(...columns: Exclude<B, I | J>[]): CreateColumnFamilyStatement<R, S, I, J, B> {
    this.nullableColumns = <any>columns;
    return <any>this;
  }

  withOptions(options: ColumnFamilyOptions) {
    this.optionsDef = options;
    return this;
  }

  withClusteringOrder(...clusteringOrders: ([J, 'ASC' | 'DESC'])[]): this {
    this.clusteringOrderDefs = clusteringOrders;
    return <any>this;
  }

  createMaterializedView(name: string | Raw): CreateMaterializedViewStatement<R, S, I, J, N>;
  createMaterializedView(keyspace: string, name: string): CreateMaterializedViewStatement<R, S, I, J, N>;
  createMaterializedView(...args: any[]) {
    const statement = new CreateMaterializedViewStatement(this.builder);
    if (args.length === 2) {
      statement.as(args[0], args[1]);
    } else {
      if (typeof this.keyspaceDef !== 'undefined') {
        statement.keyspace(this.keyspaceDef);
      }
      statement.as(args[0]);
    }
    statement.from(this.columnFamilyDef);
    return <any>statement;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error('The TABLE name not defined');
    }
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.columnFamilyDef.params);
    for (const column in this.columnDefs) {
      const def = this.columnDefs[column];
      if (def instanceof Raw) {
        params.push(...def.params);
      }
    }
    return params;
  }

  protected getQuery(): string {
    let query = `CREATE TABLE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef} (`;

    const columns: string[] = Object.keys(this.columnDefs);

    query += columns.map(column => {
      const def = this.columnDefs[column];
      const { query, params } = getColumnType(def);
      return new Raw(`"${column}" ${query}`, params);
    }).join(', ');

    const primaryKey: (string | string[])[] = [];
    if (this.partitionKeyDefs.length == 1) {
      primaryKey.push(...this.partitionKeyDefs);
    } else {
      primaryKey.push(this.partitionKeyDefs);
    }

    primaryKey.push(...this.clusteringKeyDefs);

    query += `, PRIMARY KEY (${primaryKey
      .map(column =>
        Array.isArray(column)
          ? `(${column.map(partition => `"${partition}"`).join(', ')})`
          : `"${column}"`
      ).join(', ')})`;

    query += ')';

    const withDefs: Raw[] = [];

    if (this.optionsDef != null) {
      withDefs.push(...buildOptions(this.optionsDef));
    }

    if (this.clusteringOrderDefs.length) {
      withDefs.push(new Raw(`CLUSTERING ORDER BY (${
        this.clusteringOrderDefs.map(
          ([column, order]) => `"${column}" ${order}`
        ).join(', ')
      })`));
    }

    if (withDefs.length) {
      query += ` WITH ${withDefs.join(' AND ')}`
    }

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

  isModel(data: any): data is CFModel<this> {
    return true;
  }

  isInsertInput(data: any): data is CFInsertInput<this> {
    return true;
  }

  isUpdateInput(data: any): data is CFUpdateInput<this> {
    return true;
  }

  createIndex<K extends StringKey<R>>(column: K | Raw, name?: string | Raw): CreateColumnFamilyIndexStatement;
  createIndex<K extends StringKey<R>>(column: [K | Raw, 'FULL' | 'ENTRIES' | 'KEYS' | 'VALUES'], name?: string | Raw): CreateColumnFamilyIndexStatement;
  createIndex(...args: any[]) {
    const statement = new CreateColumnFamilyIndexStatement(this.builder);
    statement.keyspace(this.keyspaceDef);
    statement.on(this.columnFamilyDef);
    if (Array.isArray(args[0])) {
      statement.column(args[0][0], args[0][1]);
    } else {
      statement.column(args[0]);
    }
    if (args[1] != null) {
      statement.as(args[1]);
    }
    return statement;
  }

  select(): SelectStatement<R, S>;
  select(selection: '*'): SelectStatement<R, S, R>;
  select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, SS extends Selection<R, infer K, infer A> ? { [K2 in A]: R[K] } : never>;
  select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, Pick<R, K>>;
  select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, { [K in A]: R[K] }>;
  select(...args: any[]): any {
    const statement = new SelectStatement(this.builder);
    if (args.length === 2) {
      statement.select(args[0], args[1]);
    } else if (args.length === 1) {
      statement.select(args[0]);
    }
    if (typeof this.keyspaceDef !== 'undefined') {
      statement.keyspace(this.keyspaceDef);
    }
    statement.from(this.columnFamilyDef);
    return statement;
  }

  insert(): InsertStatement<S>;
  insert(data: Partial<{ [K in StringKey<S>]: S[K] }>):  InsertStatement<S>;
  insert<K extends StringKey<R>>(select: K, replace: S[K]):  InsertStatement<S>;
  insert(...args: any[]): any {
    const statement = new InsertStatement(this.builder);
    if (args.length > 0) {
      if (args.length > 1) {
        (<any>statement).value(...args);
      } else {
        (<any>statement).values(...args);
      }
    }
    if (typeof this.keyspaceDef !== 'undefined') {
      statement.keyspace(this.keyspaceDef);
    }
    statement.into(this.columnFamilyDef);
    return statement;
  }

  update(): UpdateStatement<S>;
  update(data: Partial<{ [K in StringKey<S>]: S[K] }>): UpdateStatement<S>;
  update<K extends StringKey<S>>(data: ([K, S[K]])[]): UpdateStatement<S>;
  update<K extends StringKey<S>>(selection: K, value: S[K]): UpdateStatement<S>;
  update(...args: any[]): any {
    const statement = new UpdateStatement(this.builder);
    if (typeof this.keyspaceDef !== 'undefined') {
      statement.keyspace(this.keyspaceDef);
    }
    statement.on(this.columnFamilyDef);
    if (args.length > 0) {
      (<any>statement).set(...args);
    }
    return statement;
  }

  delete(): DeleteStatement<S>;
  delete<K extends StringKey<S>>(columns: K[]): DeleteStatement<S>;
  delete(...args: any): any {
    const statement = new DeleteStatement(this.builder);
    if (args.length > 0) {
      (<any>statement).columns(...args);
    }
    if (typeof this.keyspaceDef !== 'undefined') {
      statement.keyspace(this.keyspaceDef);
    }
    statement.from(this.columnFamilyDef);
    return <any>statement;
  }

}

export type CFModel<T> = T extends CreateColumnFamilyStatement<infer R, any, infer I, infer J, infer N>
  ? Pick<R, I | J | Exclude<StringKey<R>, N>> & Partial<Pick<R, Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>>>
  : (T extends CreateMaterializedViewStatement<infer R, any, infer I, infer J, infer N>
    ? Pick<R, I | J | Exclude<StringKey<R>, N>> & Partial<Pick<R, Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>>>
    : any
  );

export type CFUpdateInput<T> = T extends CreateColumnFamilyStatement<infer R, infer S, infer I, infer J, infer N>
  ? ({ [K in Exclude<StringKey<R>, N | I | J>]?: S[K] } & { [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null })
  : (T extends CreateMaterializedViewStatement<infer R, infer S, infer I, infer J, infer N>
    ? ({ [K in Exclude<StringKey<R>, N | I | J>]?: S[K] } & { [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null })
    : any
    );

export type CFInsertInput<T> = T extends CreateColumnFamilyStatement<infer R, infer S, infer I, infer J, infer N>
  ? ({ [K in I | J | Exclude<StringKey<R>, N>]: S[K] } & { [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null })
  : (T extends CreateMaterializedViewStatement<infer R, infer S, infer I, infer J, infer N>
    ? ({ [K in I | J | Exclude<StringKey<R>, N>]: S[K] } & { [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null })
    : any
  );