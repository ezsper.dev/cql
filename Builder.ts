import {
  SelectStatement,
} from './SelectStatement';
import { UpdateStatement } from './UpdateStatement';
import { InsertStatement } from './InsertStatement';
import { DeleteStatement } from './DeleteStatement';
import { CreateColumnFamilyStatement } from './CreateColumnFamilyStatement';
import { CreateKeyspaceStatement } from './CreateKeyspaceStatement';
import { CreateColumnFamilyIndexStatement } from './CreateColumnFamilyIndexStatement';
import { CreateMaterializedViewStatement } from './CreateMaterializedViewStatement';
import { Raw } from './Raw';

export interface BuilderOptions {}

export type Statement = SelectStatement
  | UpdateStatement
  | DeleteStatement
  | InsertStatement
  | CreateKeyspaceStatement
  | CreateColumnFamilyStatement
  | CreateColumnFamilyIndexStatement
  | CreateMaterializedViewStatement;

export class Builder {

  static Update = UpdateStatement;
  static Insert = InsertStatement;
  static Delete = DeleteStatement;
  static CreateKeyspace = CreateKeyspaceStatement;
  static CreateColumnFamily = CreateColumnFamilyStatement;
  static CreateColumnFamilyIndex = CreateColumnFamilyIndexStatement;
  static CreateMaterializedView = CreateMaterializedViewStatement;

  constructor(readonly options?: BuilderOptions) {}

  token(values: (any | Raw)[]): Raw;
  token(...columns: (string | Raw)[]): Raw;
  token(...args: any[]) {
    const params: any[] = [];
    if (Array.isArray(args[0])) {
      return new Raw(`TOKEN(${args[0].map((value: any) => {
        if (value instanceof Raw) {
          params.push(...value.params);
          return value;
        } else {
          params.push(value);
          return '?';
        }
      }).join(', ')})`, params);
    }
    return new Raw(`TOKEN(${args.map(column => {
      if (column instanceof Raw) {
        params.push(...column.params);
        return column;
      }
      return `"${column}"`;
    }).join(', ')})`, params);
  }

  now() {
    return new Raw('NOW()');
  }

  ttl(column: string | Raw) {
    const params: any[] = [];
    if (column instanceof Raw) {
      params.push(...column.params);
    }
    return new Raw(`TTL(${column instanceof Raw ? column : `"${column}"`})`, params);
  }

  timestamp(column: string | Raw) {
    const params: any[] = [];
    if (column instanceof Raw) {
      params.push(...column.params);
    }
    return new Raw(`TIMESTAMP(${column instanceof Raw ? column : `"${column}"`})`, params);
  }

  key(column: string | Raw, key: any) {
    const params: any[] = [];
    if (column instanceof Raw) {
      params.push(...column.params);
    }
    params.push(key);
    return new Raw(`${column instanceof Raw ? column : `"${column}"`}[?]`, params);
  }

  createKeyspace(name: string | Raw) {
    const statement = new CreateKeyspaceStatement(this);
    statement.as(name);
    return statement;
  }

  createIndex(column: string | Raw, name?: string | Raw): CreateColumnFamilyIndexStatement;
  createIndex(column: [string | Raw, 'FULL' | 'ENTRIES' | 'KEYS' | 'VALUES'], name?: string | Raw): CreateColumnFamilyIndexStatement;
  createIndex(...args: any[]) {
    const statement = new CreateColumnFamilyIndexStatement(this);
    if (Array.isArray(args[0])) {
      statement.column(args[0][0], args[0][1]);
    } else {
      statement.column(args[0]);
    }
    if (args[1] != null) {
      statement.as(args[1]);
    }
    return statement;
  }

  createColumnFamily(name: string | Raw): CreateColumnFamilyStatement;
  createColumnFamily(keyspace: string, name: string): CreateColumnFamilyStatement;
  createColumnFamily(...args: any[]) {
    const statement = new CreateColumnFamilyStatement(this);
    if (args.length === 2) {
      statement.as(args[0], args[1]);
    } else {
      statement.as(args[0]);
    }
    return statement;
  }

  createMaterializedView(name: string | Raw): CreateMaterializedViewStatement;
  createMaterializedView(keyspace: string, name: string): CreateMaterializedViewStatement;
  createMaterializedView(...args: any[]) {
    const statement = new CreateMaterializedViewStatement(this);
    if (args.length === 2) {
      statement.as(args[0], args[1]);
    } else {
      statement.as(args[0]);
    }
    return statement;
  }

  select(name: string | Raw): SelectStatement;
  select(keyspace: string, name: string): SelectStatement;
  select(): SelectStatement;
  select(...args: any[]) {
    const statement = new SelectStatement(this);
    if (args.length === 2) {
      statement.from(args[0], args[1]);
    } else if (args.length === 1) {
      statement.from(args[0]);
    }
    return statement;
  }

  insert(name: string | Raw): InsertStatement;
  insert(keyspace: string, name: string): InsertStatement;
  insert(): InsertStatement;
  insert(...args: any[]) {
    const statement = new InsertStatement(this);
    if (args.length === 2) {
      statement.into(args[0], args[1]);
    } else if (args.length === 1) {
      statement.into(args[0]);
    }
    return statement;
  }

  update(name: string | Raw): UpdateStatement;
  update(keyspace: string, name: string): UpdateStatement;
  update(): UpdateStatement;
  update(...args: any[]) {
    const statement = new UpdateStatement(this);
    if (args.length === 2) {
      statement.on(args[0], args[1]);
    } else if (args.length === 1) {
      statement.on(args[0]);
    }
    return statement;
  }

  delete(name: string | Raw): DeleteStatement;
  delete(keyspace: string, name: string): DeleteStatement;
  delete(): DeleteStatement;
  delete(...args: any[]) {
    const statement = new DeleteStatement(this);
    if (args.length === 2) {
      statement.from(args[0], args[1]);
    } else if (args.length === 1) {
      statement.from(args[0]);
    }
    return statement;
  }

  filterResult(result: any): any {
    return result;
  }

  mapRow(index: number, row: any): any {
    return row;
  }

}