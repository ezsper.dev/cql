import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
import {
  StringKey,
  TransformPossibleColumnDefinition,
} from './ColumnDefinition';

export type WhereEntry<S extends TransformPossibleColumnDefinition, K extends StringKey<S>> = ([K, '>' | '<' | '=' | 'CONTAINS', S[K]])
  | ([K, 'IS NOT NULL'])
  | ([K, 'IN', (Raw | any)[]])
  | ([[K], '>' | '<' | '=', [S[K]][]]);

export type WhereEntryMulti2<S extends TransformPossibleColumnDefinition, K1 extends StringKey<S>, K2 extends StringKey<S>> =
  | ([[K1, K2], '>' | '<' | '=', [S[K1], S[K2]][]]);

export type WhereEntryMulti3<S extends TransformPossibleColumnDefinition, K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>> =
  | ([[K1, K2, K3], '>' | '<' | '=', [S[K1], S[K2], S[K3]][]]);

export type WhereEntryMulti4<S extends TransformPossibleColumnDefinition, K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>> =
  | ([[K1, K2, K3, K4], '>' | '<' | '=', [S[K1], S[K2], S[K3], S[K4]][]]);

export type WhereEntryMulti5<S extends TransformPossibleColumnDefinition, K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>> =
  | ([[K1, K2, K3, K4, K5], '>' | '<' | '=', [S[K1], S[K2], S[K3], S[K4], S[K5]][]]);

export class WhereStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, P extends { [name: string]: any } = {}> extends BaseStatement<P> {

  protected whereClauseDefs: Raw[] = [];

  where(selection: Raw): this;
  where<K extends StringKey<S>>(keys: [K], comparator: 'IN', valueList: [S[K]][]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], comparator: 'IN', valueList: [S[K1], S[K2]][]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], comparator: 'IN', valueList: [S[K1], S[K2], S[K3]][]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], comparator: 'IN', valueList: [S[K1], S[K2], S[K3], S[K4]][]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], comparator: 'IN', valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]][]): this;
  where<K extends StringKey<S>>(keys: [K], comparator: '>' | '<' | '=' | '>=' | '<=', valueList: [S[K]]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], comparator: '>' | '<' | '=' | '>=' | '<=', valueList: [S[K1], S[K2]]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], comparator: '>' | '<' | '=' | '>=' | '<=', valueList: [S[K1], S[K2], S[K3]]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], comparator: '>' | '<' | '=' | '>=' | '<=', valueList: [S[K1], S[K2], S[K3], S[K4]]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], comparator: '>' | '<' | '=' | '>=' | '<=', valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]]): this;
  where<K extends StringKey<S>>(selection: WhereEntry<S, K>[]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>>(selection: WhereEntryMulti2<S, K1, K2>[]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(selection: WhereEntryMulti3<S, K1, K2, K3>[]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(selection: WhereEntryMulti4<S, K1, K2, K3, K4>[]): this;
  where<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(selection: WhereEntryMulti5<S, K1, K2, K3, K4, K5>[]): this;
  where(selection: Partial<{ [J in StringKey<S>]: S[J] }>): this;
  where<K extends StringKey<S>>(key: K, comparator: 'IS NOT NULL'): this;
  where<K extends StringKey<S>>(key: K, comparator: 'IN', values: S[K][]): this;
  where<K extends StringKey<S>>(key: K, comparator: '>' | '<' | '=' | '>=' | '<=' | 'CONTAINS', value: S[K]): this;
  where(...args: any[]): this {
    if (args.length > 1) {
      const params: any[] = [];
      const [ selection, comparator, value ] = args;
      const joinValues = (values: any[]) => `(${values.map((def: any) => {
        if (Array.isArray(selection) && comparator === 'IN') {
          def.forEach((defItem: any) => {
            if (defItem instanceof Raw) {
              params.push(...defItem.params);
              return def.query;
            }
            params.push(defItem);
          });
          return `(${selection.map(() => '?').join(', ')})`;
        }
        if (def instanceof Raw) {
          params.push(...def.params);
          return def.query;
        }
        params.push(def);
        return '?';
      }).join(', ')})`;
      if (Array.isArray(selection)) {
        const query = `(${selection.map((def: any) => {
          if (def instanceof Raw) {
            params.push(...def.params);
            return def.query;
          }
          return `"${def}"`;
        }).join(', ')}) ${comparator} ${joinValues(value)}`;
        this.whereClauseDefs.push(new Raw(query, params));
      } else {
        const params: any[] = [];
        if (selection instanceof Raw) {
          params.push(...selection.params);
        }
        if (value instanceof Raw) {
          params.push(...value.params);
        } else if (typeof value !== 'undefined') {
          if (comparator === 'IN') {
            params.push(...value);
          } else {
            params.push(value);
          }
        }
        const query = `${
          selection instanceof Raw
            ? selection.query
            : `"${selection}"`
        } ${comparator}${
          comparator === 'IN'
            ? ` ${joinValues(value)}`
            : (comparator === 'IS NOT NULL'
              ? ''
              : ` ${(value instanceof Raw ? value.query : '?')}`
          )
        }`;
        this.whereClauseDefs.push(new Raw(query, params));
      }
      return this;
    }
    if (args[0] instanceof Raw) {
      this.whereClauseDefs.push(args[0]);
    } else if (Array.isArray(args[0])) {
      (<any>this).where(...args[0]);
    } else {
      for (const key in args[0]) {
        this.where(<any>key, '=', args[0][key]);
      }
    }
    return this;
  }

  whereIsNotNull<K extends StringKey<S>>(key: K) {
    this.where(key, 'IS NOT NULL');
    return this;
  }

  whereIn(where: { [K in StringKey<S>]: S[K] }[]): this;
  whereIn<K extends StringKey<S>>(keys: [K], valueList: [S[K]][]): this;
  whereIn<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], valueList: [S[K1], S[K2]][]): this;
  whereIn<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], valueList: [S[K1], S[K2], S[K3]][]): this;
  whereIn<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], valueList: [S[K1], S[K2], S[K3], S[K4]][]): this;
  whereIn<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]][]): this;
  whereIn<K extends StringKey<S>>(keys: K[], valueList: any[][]): this;
  whereIn<K extends StringKey<S>>(key: K, values: S[K][]): this;
  whereIn(...args: any[]) {
    if (args.length === 2) {
      this.where(args[0], 'IN', args[1]);
    } else {
      const keys = Object.keys(args[0]);
      const values = Object.values(args[0]);
      return (<any>this).where(keys, 'IN', values);
    }
    return this;
  }

  whereContains<K extends StringKey<S>>(keys: [K], valueList: [S[K]]): this;
  whereContains<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], valueList: [S[K1], S[K2]]): this;
  whereContains<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], valueList: [S[K1], S[K2], S[K3]]): this;
  whereContains<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], valueList: [S[K1], S[K2], S[K3], S[K4]]): this;
  whereContains<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]]): this;
  whereContains<K extends StringKey<S>>(keys: K[], valueList: S[K][]): this;
  whereContains<K extends StringKey<S>>(key: K, value: S[K]): this;
  whereContains(key: any, value: any) {
    this.where(key, 'CONTAINS', value);
    return this;
  }

  whereLowerThan<K extends StringKey<S>>(keys: [K], valueList: [S[K]]): this;
  whereLowerThan<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], valueList: [S[K1], S[K2]]): this;
  whereLowerThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], valueList: [S[K1], S[K2], S[K3]]): this;
  whereLowerThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], valueList: [S[K1], S[K2], S[K3], S[K4]]): this;
  whereLowerThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]]): this;
  whereLowerThan<K extends StringKey<S>>(keys: K[], valueList: any[]): this;
  whereLowerThan<K extends StringKey<S>>(key: K, value: S[K]): this;
  whereLowerThan(key: any, value: any) {
    this.where(key, '<', value);
    return this;
  }

  whereGreaterThan<K extends StringKey<S>>(keys: [K], valueList: [S[K]]): this;
  whereGreaterThan<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], valueList: [S[K1], S[K2]]): this;
  whereGreaterThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], valueList: [S[K1], S[K2], S[K3]]): this;
  whereGreaterThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], valueList: [S[K1], S[K2], S[K3], S[K4]]): this;
  whereGreaterThan<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]]): this;
  whereGreaterThan<K extends StringKey<S>>(keys: K[], valueList: any[]): this;
  whereGreaterThan<K extends StringKey<S>>(key: K, value: S[K]): this;
  whereGreaterThan(key: any, value: any) {
    this.where(key, '>', value);
    return this;
  }

  whereEquals<K extends StringKey<S>>(keys: [K], valueList: [S[K]]): this;
  whereEquals<K1 extends StringKey<S>, K2 extends StringKey<S>>(keys: [K1, K2], valueList: [S[K1], S[K2]]): this;
  whereEquals<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>>(keys: [K1, K2, K3], valueList: [S[K1], S[K2], S[K3]]): this;
  whereEquals<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>>(keys: [K1, K2, K3, K4], valueList: [S[K1], S[K2], S[K3], S[K4]]): this;
  whereEquals<K1 extends StringKey<S>, K2 extends StringKey<S>, K3 extends StringKey<S>, K4 extends StringKey<S>, K5 extends StringKey<S>>(keys: [K1, K2, K3, K4, K5], valueList: [S[K1], S[K2], S[K3], S[K4], S[K5]]): this;
  whereEquals<K extends StringKey<S>>(keys: K[], valueList: any[]): this;
  whereEquals<K extends StringKey<S>>(key: K, value: S[K]): this;
  whereEquals(key: any, value: any) {
    this.where(key, '=', value);
    return this;
  }

  hasWhereClauses() {
    return this.whereClauseDefs.length > 0;
  }

  getWhereParams(): any[] {
    const params: any[] = [];
    for (const def of this.whereClauseDefs) {
      params.push(...def.params);
    }
    return params;
  }

  getWhereQuery(): string {
    if (!this.hasWhereClauses()) {
      return '';
    }

    let query = ' WHERE ';
    query += this.whereClauseDefs.map(where => where.query).join(' AND ');
    return query;
  }

}
