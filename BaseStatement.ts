import * as driver from 'cassandra-driver';
import { Builder } from './Builder';
import { Raw } from './Raw';
import { StringKey } from './ColumnDefinition';

export type Row<T> = {
  get<K extends StringKey<T>>(column: K): { [K2 in K]: T[K2]; };
  values(): T[];
  keys(): keyof T;
  forEach(callback: driver.Callback): void;
} & { [K in StringKey<T>]: T[K] };

export interface ResultSet<T> {
  info: {
    queriedHost: driver.Host,
    triedHosts: { [key: string]: string; },
    speculativeExecutions: number,
    achievedConsistency: driver.types.consistencies,
    traceId: driver.types.Uuid,
    warnings: string[],
    customPayload: any
  };
  rows: Row<T>[];
  rowLength: number;
  columns: { [K in StringKey<T>]: string; }[];
  pageState: string;
  nextPage: Function;

  first(): Row<T> | null;
  getPageState(): string;
  getColumns(): { [K in StringKey<T>]: string; }[];
  wasApplied(): boolean;
  [Symbol.iterator](): Iterator<Row<T>>;
}

export interface EachRowIterator<T> {
  (index: number, row: Row<T>): any;
}

export interface EachRowCallback<T> {
  (error?: Error): void;
}

export abstract class BaseStatement<P extends { [name: string]: any } = {}> {

  constructor(protected builder: Builder) {}

  build(): Raw {
    throw new Error('Cannot execute BaseStatement');
  }

  async execute(client: driver.Client, options?: driver.QueryOptions): Promise<ResultSet<P>> {
    const { query, params } = this.build();
    const result = this.builder.filterResult(
      client.execute(query, params, { ...options, prepare: true }),
    );
    result.rows = (<any[]>result.rows).map((row, index) => this.builder.mapRow(index, row));
    return <any>result;
  }

  async eachRown(client: driver.Client, options?: driver.QueryOptions, iterator?: EachRowIterator<P>, callback?: EachRowCallback<P>): Promise<void> {
    const { query, params } = this.build();
    client.eachRow(
      query,
      params,
      { ...options, prepare: true },
      (index: number, row: any) => {
        if (typeof iterator === 'function') {
          iterator(index, this.builder.mapRow(index, row));
        }
      },
      callback,
    );
  }

}
