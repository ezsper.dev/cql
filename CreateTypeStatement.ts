import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
import {
  ColumnDefinition,
  TransformColumnDefinition,
  TransformPossibleColumnDefinition,
  getColumnType,
} from './ColumnDefinition';

export class CreateTypeStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends BaseStatement {

  protected keyspaceDef: Raw;
  protected ifNotExistsDef: boolean;
  protected columnFamilyDef: Raw;
  protected columnDefs: ColumnDefinition;

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  as(name: string | Raw): this;
  as(keyspace: string | Raw, name: string | Raw): this;
  as(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  ifNotExists(value = true) {
    this.ifNotExistsDef = value;
    return this;
  }

  columns<B extends ColumnDefinition>(columns: B): CreateTypeStatement<TransformColumnDefinition<B>, TransformPossibleColumnDefinition<B>> {
    this.columnDefs = Object.assign({}, this.columnDefs, columns);
    return <any>this;
  }

  getColumnFamilyDef(): Readonly<Raw> {
    return this.columnFamilyDef;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error('The TABLE name not defined');
    }
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.columnFamilyDef.params);
    for (const column in this.columnDefs) {
      const def = this.columnDefs[column];
      if (def instanceof Raw) {
        params.push(...def.params);
      }
    }
    return params;
  }

  protected getQuery(): string {
    let query = `CREATE TYPE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef} (`;

    const columns: string[] = Object.keys(this.columnDefs);

    query += columns.map(column => {
      const def = this.columnDefs[column];
      const { query, params } = getColumnType(def);
      return new Raw(`"${column}" ${query}`, params);
    }).join(', ');

    query += ')';

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}