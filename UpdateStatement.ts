import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import { unix } from './utils/unix';
import {
  StringKey,
  TransformPossibleColumnDefinition,
} from './ColumnDefinition';

export class UpdateStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends WhereStatement<S> {

  protected keyspaceDef: Raw;
  protected columnFamilyDef: Raw;
  protected setClauseDefs: Raw[] = [];
  protected ttl: number;
  protected timestamp: number;

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  on(name: string | Raw): this;
  on(keyspace: string | Raw, name: string | Raw): this;
  on(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  protected setValue<K extends StringKey<S>>(
    selection: K,
    operator: '+' | '=' | '-',
    value: S[K],
  ): this {
    const params: any[] = [];
    let query: string;
    if (Array.isArray(selection)) {
      query = `"${selection[0]}"[?]`;
      params.push(selection[1], selection[1]);
    } else {
      query = `"${selection}"`;
    }
    params.push(value);
    if (operator === '=') {
      query = `${query} = ?`;
    } else {
      query = `${query} = ${query} ${operator} ?`;
    }
    this.setClauseDefs.push(new Raw(query, params));
    return this;
  }

  set(data: Partial<{ [K in StringKey<S>]: S[K] | null }>): this
  set<K extends StringKey<S>>(selection: K, value: S[K] | null): this;
  set(...args: any[]) {
    if (args.length === 1) {
      if (Array.isArray(args[0])) {
        for (const [selection, value] of args[0]) {
          this.set(selection, value);
        }
      } else if (typeof args[0] === 'object' && args[0] != null) {
        for (const column in args[0]) {
          this.set(<any>column, args[0][column]);
        }
      }
      return this;
    }
    return this.setValue(args[0], '=', args[1]);
  }

  add<K extends StringKey<S>>(selection: K, value: S[K]): this {
    return this.setValue(selection, '+', value);
  }

  remove<K extends StringKey<S>>(selection: K, value: S[K]): this {
    return this.setValue(selection, '-', value);
  }

  increment<K extends StringKey<S>>(selection: K, value: S[K]): this {
    return this.setValue(selection, '+', value);
  }

  decrement<K extends StringKey<S>>(selection: K, value: S[K]): this {
    return this.setValue(selection, '-', value);
  }

  usingTTL(unix: number): this;
  usingTTL(date: Date): this;
  usingTTL(interval: string): this;
  usingTTL(value: number | Date | string) {
    this.ttl = unix(value, true);
    return this;
  }

  usingTimestamp(unix: number): this;
  usingTimestamp(date: Date): this;
  usingTimestamp(interval: string): this;
  usingTimestamp(value: number | Date | string) {
    this.timestamp = unix(value);
    return this;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error(`No UPDATE column family was defined`);
    }
    if (this.setClauseDefs.length === 0) {
      throw new Error('No SET clause was applied');
    }
    return this;
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.columnFamilyDef.params);
    for (const def of this.setClauseDefs) {
      params.push(...def.params);
    }
    params.push(...this.getWhereParams());
    return params;
  }

  protected getQuery(): string {
    const usingDefs: string[] = [];
    if (this.ttl != null) {
      usingDefs.push(`TTL ${this.ttl}`);
    }
    if (this.timestamp != null) {
      usingDefs.push(`TIMESTAMP ${this.timestamp}`);
    }
    let query = `UPDATE ${
      typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''
    }${this.columnFamilyDef}${usingDefs.length > 0 ? ` USING ${usingDefs.join(' AND ')}` : ''} SET ${this.setClauseDefs.join(', ')}`;
    query += this.getWhereQuery();
    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}
