export class Raw {
  constructor(
    public readonly query: string,
    public readonly params: any[] = [],
  ) {}

  toString() {
    return this.query;
  }
}

export function raw(strings: TemplateStringsArray, ...values: any[]): Raw {
  const params: any[] = [];
  let query = String.raw(
    strings,
    ...values.map((value: any) => {
      if (value instanceof Raw) {
        params.push(...value.params);
        return value.query;
      }
      params.push(value);
      return '?';
    }),
  );
  return new Raw(query  , params);
}
