import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
import { unix } from './utils/unix';
import {
  StringKey,
  TransformPossibleColumnDefinition,
} from './ColumnDefinition';

export class InsertStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends BaseStatement {

  protected columnDefs: Raw[] = [];
  protected valueDefs: Raw[] = [];
  protected keyspaceDef: Raw;
  protected columnFamilyDef: Raw;
  protected ttl: number;
  protected timestamp: number;

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  into(name: string | Raw): this;
  into(keyspace: string | Raw, name: string | Raw): this;
  into(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  values(data: Partial<{ [K in StringKey<S>]: S[K] }>): this {
    for (const column in (<any>data)) {
      if ((<any>data).hasOwnProperty(column)) {
        this.columnDefs.push(new Raw(`"${column}"`));
        this.valueDefs.push((<any>data)[column] instanceof Raw ? (<any>data)[column] : new Raw(`?`, [(<any>data)[column]]));
      }
    }
    return this;
  }

  value<K extends StringKey<S>>(column: K, value: S[K]): this {
    this.columnDefs.push(new Raw(`"${column}"`));
    this.valueDefs.push(new Raw(`?`, [value]));
    return this;
  }

  usingTTL(unix: number): this;
  usingTTL(date: Date): this;
  usingTTL(interval: string): this;
  usingTTL(value: number | Date | string) {
    this.ttl = unix(value, true);
    return this;
  }

  usingTimestamp(unix: number): this;
  usingTimestamp(date: Date): this;
  usingTimestamp(interval: string): this;
  usingTimestamp(value: number | Date | string) {
    this.timestamp = unix(value);
    return this;
  }

  protected validate() {
    if (this.columnDefs.length === 0) {
      throw new Error(`No COLUMNS were defined`);
    }
    if (this.valueDefs.length === 0) {
      throw new Error(`No VALUES were defined`);
    }
    if (this.columnDefs.length !== this.valueDefs.length) {
      throw new Error(`The columns and values must match on length`);
    }
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.columnFamilyDef.params);
    for (const def of this.columnDefs) {
      params.push(...def.params);
    }
    for (const def of this.valueDefs) {
      params.push(...def.params);
    }
    return params;
  }

  protected getQuery(): string {
    const usingDefs: string[] = [];
    if (this.ttl != null) {
      usingDefs.push(`TTL ${this.ttl}`);
    }
    if (this.timestamp != null) {
      usingDefs.push(`TIMESTAMP ${this.timestamp}`);
    }

    let query = `INSERT INTO ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef}`;

    query += ` (${this.columnDefs.join(', ')})`;

    query += ` VALUES (${this.valueDefs.join(', ')})`;

    if (usingDefs.length > 0) {
      query += ` USING ${usingDefs.join(' AND ')}`;
    }

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}
