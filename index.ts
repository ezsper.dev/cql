import { Builder } from './Builder';

export * from './Raw';
export * from './Type';
export { Type as type } from './Type';
export * from './ColumnDefinition';
export * from './BaseStatement';
export * from './SelectStatement';
export * from './UpdateStatement';
export * from './InsertStatement';
export * from './DeleteStatement';
export * from './CreateKeyspaceStatement';
export * from './CreateColumnFamilyStatement';
export * from './CreateColumnFamilyIndexStatement';
export * from './CreateMaterializedViewStatement';
export * from './CreateTypeStatement';
export * from './Builder';

const builder = new Builder();

let {
  token,
  ttl,
  now,
  timestamp,
  delete: deleteRow,
  select,
  update,
  insert,
  createKeyspace,
  createColumnFamily,
  createMaterializedView,
  createIndex,
} = builder;

token = token.bind(builder);
ttl = ttl.bind(builder);
now = now.bind(builder);
timestamp = timestamp.bind(builder);
deleteRow = deleteRow.bind(builder);
select = select.bind(builder);
update = update.bind(builder);
insert = insert.bind(builder);
createKeyspace = createKeyspace.bind(builder);
createColumnFamily = createColumnFamily.bind(builder);
createMaterializedView = createMaterializedView.bind(builder);
createIndex = createIndex.bind(builder);

export {
  token,
  ttl,
  now,
  timestamp,
  deleteRow,
  select,
  update,
  insert,
  createKeyspace,
  createColumnFamily,
  createMaterializedView,
  createIndex,
};
