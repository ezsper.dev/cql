import { TypeDefinition } from './Type';

export type TransformType<T extends TypeDefinition> = any;

export type TransformPossibleType<T extends TypeDefinition> = any;