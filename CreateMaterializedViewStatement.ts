import { SelectStatement } from './SelectStatement';
import { InsertStatement } from './InsertStatement';
import { UpdateStatement } from './UpdateStatement';
import { DeleteStatement } from './DeleteStatement';
import { WhereStatement } from './WhereStatement';
import { buildOptions } from './CreateKeyspaceStatement';
import { Raw } from './Raw';
import {
  TransformColumnDefinition,
  TransformPossibleColumnDefinition,
  Selection,
  StringKey,
} from './ColumnDefinition';

export interface MaterializedViewOptions {
  bloomFilterFpChance?: number;
  comment?: string;
  crcCheckChance?: number;
  dclocalReadRepairChange?: number;
  defaultTimeToLive?: number;
  gcGraceSeconds?: number;
  maxIndexInterval?: number;
  memtableFlushPeriodInMs?: number;
  minIndexInterval?: number;
  readRepairChance?: number;
  speculativeRetry?: string;
  caching?: {
    keys?: string;
    rowsPerPartition?: string;
  };
  compression?: {
    chunkLengthInKb?: number;
    class?: string;
    enabled?: boolean;
  };
  compaction?: {
    class?: string;
    maxThreshold?: number;
    minThreshold?: number;
  };
  [name: string]: any;
}
export class CreateMaterializedViewStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, I extends StringKey<R & S> = never, J extends StringKey<R & S> = never, N extends string = never> extends WhereStatement<S> {

  static Update = UpdateStatement;
  static Insert = InsertStatement;
  static Delete = DeleteStatement;

  protected materializedViewDef: Raw;
  protected ifNotExistsDef: boolean;
  protected keyspaceDef: Raw;
  protected optionsDef: MaterializedViewOptions;
  protected columnFamilyDef: Raw;
  protected columnDefs: (keyof R)[] = [];
  protected partitionKeyDefs: I[] = [];
  protected clusteringKeyDefs: J[] = [];
  protected clusteringOrderDefs: ([J, 'ASC' | 'DESC'])[] = [];

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  as(name: string | Raw): this;
  as(keyspace: string | Raw, name: string | Raw): this;
  as(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.materializedViewDef = args[0];
      } else {
        this.materializedViewDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.materializedViewDef = args[1];
      } else {
        this.materializedViewDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  withOptions(options: MaterializedViewOptions) {
    this.optionsDef = options;
    return this;
  }

  ifNotExists(value = true) {
    this.ifNotExistsDef = value;
    return this;
  }

  from(name: string | Raw): this {
    this.columnFamilyDef = name instanceof Raw ? name : new Raw(`"${name}"`);
    return this;
  }

  columns(columns: '*'): this;
  columns<B extends StringKey<R & S>>(...columns: B[]): CreateMaterializedViewStatement<Pick<R, B>, Pick<S, B>, never, never, N>;
  columns(...args: any[]): any {
    if (args[0] === '*') {
      this.columnDefs = [];
    } else {
      this.columnDefs = args;
    }
    return <any>this;
  }

  partitionKeys<B extends StringKey<R>>(...columns: B[]): CreateMaterializedViewStatement<R, S, B, never, N> {
    this.partitionKeyDefs = <any>columns;
    for (const column of columns) {
      (<any>this).whereIsNotNull(column);
    }
    return <any>this;
  }

  clusteringKeys<B extends StringKey<R>>(...columns: B[]): CreateMaterializedViewStatement<R, S, I, B, N> {
    this.clusteringKeyDefs = <any>columns;
    for (const column of columns) {
      (<any>this).whereIsNotNull(column);
    }
    return <any>this;
  }

  withClusteringOrder(...clusteringOrders: ([J, 'ASC' | 'DESC'])[]): this {
    this.clusteringOrderDefs = clusteringOrders;
    return <any>this;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error('The TABLE name not defined');
    }
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.materializedViewDef.params);
    params.push(...this.columnFamilyDef.params);
    params.push(...this.getWhereParams());
    return params;
  }

  protected getQuery(): string {
    let query = `CREATE MATERIALIZED VIEW ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.materializedViewDef} AS SELECT `;

    if (this.columnDefs.length) {
      query += this.columnDefs.map(column => `"${column}"`).join(', ');
    } else {
      query += '*';
    }

    query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef}`;

    query += this.getWhereQuery();

    const primaryKey: (string | string[])[] = [];
    if (this.partitionKeyDefs.length === 1) {
      primaryKey.push(...(<any[]>this.partitionKeyDefs));
    } else {
      primaryKey.push(<any[]>this.partitionKeyDefs);
    }

    primaryKey.push(...(<any[]>this.clusteringKeyDefs));

    query += ` PRIMARY KEY (${primaryKey
      .map(column =>
        Array.isArray(column)
          ? `(${column.map(partition => `"${partition}"`).join(', ')})`
          : `"${column}"`
      ).join(', ')})`;

    const withDefs: Raw[] = [];

    if (this.optionsDef != null) {
      withDefs.push(...buildOptions(this.optionsDef));
    }

    if (this.clusteringOrderDefs.length) {
      withDefs.push(new Raw(`CLUSTERING ORDER BY (${
        this.clusteringOrderDefs.map(
          ([column, order]) => `"${column}" ${order}`
        ).join(', ')
      })`));
    }

    if (withDefs.length) {
      query += ` WITH ${withDefs.join(' AND ')}`
    }

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

  select(): SelectStatement<R, S>;
  select(selection: '*'): SelectStatement<R, S, R>;
  select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, SS extends Selection<R, infer K, infer A> ? { [K2 in A]: R[K] } : never>;
  select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, Pick<R, K>>;
  select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, { [K in A]: R[K] }>;
  select(...args: any[]): any {
    const statement = new SelectStatement(this.builder);
    if (args.length === 2) {
      statement.select(args[0], args[1]);
    } else if (args.length === 1) {
      statement.select(args[0]);
    }
    if (typeof this.keyspaceDef !== 'undefined') {
      statement.keyspace(this.keyspaceDef);
    }
    statement.from(this.materializedViewDef);
    return statement;
  }

}
