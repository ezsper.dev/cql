import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';

export class CreateColumnFamilyIndexStatement extends BaseStatement {

  protected secondaryIndexDef: Raw;
  protected columnFamilyDef: Raw;
  protected columnDef: Raw;
  protected keyspaceDef: Raw;
  protected ifNotExistsDef: boolean;

  ifNotExists(value = true) {
    this.ifNotExistsDef = value;
    return this;
  }

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  as(name: string | Raw) {
    this.secondaryIndexDef = name instanceof Raw ? name : new Raw(`${name}`);
    return this;
  }

  on(name: string | Raw): this;
  on(keyspace: string | Raw, name: string | Raw): this;
  on(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  column(name: string | Raw, type?: 'FULL' | 'ENTRIES' | 'VALUES' | 'KEYS') {
    const params: any[] = [];
    if (name instanceof Raw) {
      params.push(...name.params);
    }
    if (type == null) {
      this.columnDef = new Raw(`"${name}"`, params);
    } else {
      let column: string;
      if (name instanceof Raw) {
        column = name.query;
      } else {
        column = name;
      }
      this.columnDef = new Raw(`${type}(${column})`, params);
    }
    return this;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error('The ON name was not defined');
    }
    if (this.columnDef == null) {
      throw new Error('The COLUMN name was not defined');
    }
  }

  protected getParams() {
    const params: any[] = [];
    if (this.secondaryIndexDef != null) {
      params.push(...this.secondaryIndexDef.params);
    }
    if (this.keyspaceDef != null) {
      params.push(...this.keyspaceDef.params);
    }
    params.push(...this.columnFamilyDef.params);
    params.push(...this.columnDef.params);
    return params;
  }

  protected getQuery(): string {
    let query = `CREATE INDEX ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}`;
    query += `${typeof this.secondaryIndexDef !== 'undefined' ? `${this.secondaryIndexDef} ` : ''}ON `;
    query += `${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef}`;
    query += ` ( ${this.columnDef} )`;
    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}
