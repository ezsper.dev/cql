import { BaseStatement } from './BaseStatement';
import { CreateColumnFamilyStatement } from './CreateColumnFamilyStatement';
import { Raw } from './Raw';

const snakeCase = require('lodash.snakecase');

export interface KeyspaceColumnFamilies {
  [name: string]: CreateColumnFamilyStatement;
}

export interface KeyspaceReplicationSimpleStrategyOptions {
  class: 'SimpleStrategy',
  replicationFactor: number,
}

export interface KeyspaceReplicationNetworkTopologyStrategyOptions {
  class: 'NetworkTopologyStrategy';
  replicationFactor: { [datacenter: string]: number };
}

export type KeyspaceReplicationOptions = KeyspaceReplicationSimpleStrategyOptions
  | KeyspaceReplicationNetworkTopologyStrategyOptions;

export interface KeyspaceOptions {
  durableWrites?: boolean;
  replication?: KeyspaceReplicationOptions;
  [name: string]: any;
}

export function buildOptionValue(value: any): string {
  if (typeof value === 'object' && value != null) {
    const options: string[] = [];
    for (const key in value) {
      options.push(`'${snakeCase(key)}': ${buildOptionValue(value[key])}`);
    }
    return `{ ${options.join(', ')} }`;
  }
  if (Array.isArray(value)) {
    return `[${value.map(buildOptionValue).join(', ')}]`;
  }
  return JSON.stringify(value).replace(/(^\"|\"$)/g, '\'');
}

export function buildOptions(options: {[name: string]: any}): Raw[] {
  const keys: Raw[] = [];
  for (const key in options) {
    keys.push(new Raw(`${snakeCase(key).toUpperCase()} = ${buildOptionValue(options[key])}`));
  }
  return keys;
}

export class CreateKeyspaceStatement<C extends KeyspaceColumnFamilies = {}> extends BaseStatement {

  static CreateColumnFamily = CreateColumnFamilyStatement;

  protected keyspaceDef: Raw;
  protected columnDefs: C;
  protected optionsDef: KeyspaceOptions;
  protected ifNotExistsDef: boolean;

  as(name: string | Raw): this;
  as(...args: any[]) {
    if (args.length === 1 && args[0] instanceof Raw) {
      this.keyspaceDef = args[0];
      return this;
    }
    let definition = `"${args[0]}"`;
    this.keyspaceDef = new Raw(definition);
    return this;
  }

  withOptions(options: KeyspaceOptions) {
    this.optionsDef = options;
    return this;
  }

  createColumnFamily(name: string | Raw): CreateColumnFamilyStatement {
    const statement = new CreateColumnFamilyStatement(this.builder);
    statement.keyspace(this.keyspaceDef);
    statement.as(name);
    return statement;
  }

  ifNotExists(value = true) {
    this.ifNotExistsDef = value;
    return this;
  }

  protected validate() {
    if (this.keyspaceDef == null) {
      throw new Error('The KEYSPACE name was not defined');
    }
    if (this.optionsDef == null || this.optionsDef.replication == null) {
      throw new Error('The REPLICATION options were not defined');
    }
  }

  protected getParams() {
    const params: any[] = [];
    params.push(...this.keyspaceDef.params);
    return params;
  }

  protected getQuery(): string {
    let query = `CREATE KEYSPACE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${this.keyspaceDef}`;

    const withDefs: Raw[] = [];

    if (this.optionsDef != null) {
      const options = {...this.optionsDef};
      if (options.replication != null) {
        const replication: any = { ...options.replication };
        if (replication.class === 'NetworkTopologyStrategy') {
          Object.assign(replication, replication.replicationFactor);
          delete replication.replicationFactor;
        }
        options.replication = replication;
      }
      withDefs.push(...buildOptions(options));
    }

    if (withDefs.length) {
      query += ` WITH ${withDefs.join(' AND ')}`
    }

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}
