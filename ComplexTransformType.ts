import { CreateTypeStatement } from './CreateTypeStatement';
import {
  ListDefinition,
  MapDefinition,
  SetDefinition,
  TupleDefinition,
  TypeDefinition,
} from './Type';
import {
  ColumnDefinition,
  FormatList,
  FormatMap,
  FormatSet,
  FormatTuple,
  FormatPossibleList,
  FormatPossibleMap,
  FormatPossibleSet,
  FormatPossibleTuple,
  NativeTypeMap, PossibleNativeTypeMap,
  StringKey
} from './ColumnDefinition';

export type TransformTypeValue<T> = T extends { type: keyof NativeTypeMap }
  ? NativeTypeMap[T['type']]
  : IComplexTransformType<T>['type'];

export interface IComplexTransformType<T> {
  type: T extends { type: infer U }
    ? (U extends keyof NativeTypeMap
      ? NativeTypeMap[U]
      : (U extends CreateTypeStatement<infer U2>
        ? (U2 extends ColumnDefinition
          ? ({ [K in keyof U2]: TransformTypeValue<U2[K]> })
          : any
          )
        : never
        )
      )
    : (T extends SetDefinition
      ? this['set']
      : (T extends ListDefinition
        ? this['list']
        : (T extends MapDefinition
          ? this['map']
          : (T extends TupleDefinition
            ? this['tuple']
            : never
            )
          )
        )
      );
  // @ts-ignore
  set: T extends SetDefinition<infer U>
    ? FormatSet<TransformTypeValue<U>>
    : never;
  // @ts-ignore
  list: T extends ListDefinition<infer U>
    ? FormatList<TransformTypeValue<U>>
    : never;
  // @ts-ignore
  map: T extends MapDefinition<infer K, infer V>
    ? FormatMap<TransformTypeValue<K>, TransformTypeValue<V>>
    : never;
  // @ts-ignore
  tuple: T extends TupleDefinition<infer U>
    ? (U extends [infer U1]
      ? FormatTuple<[TransformTypeValue<U1>]>
      : (U extends [infer U1, infer U2]
        ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>]>
        : (U extends [infer U1, infer U2, infer U3]
          ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>]>
          : (U extends [infer U1, infer U2, infer U3, infer U4]
            ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>]>
            : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5]
              ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>]>
              : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6]
                ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>, TransformTypeValue<U6>]>
                : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7]
                  ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>, TransformTypeValue<U6>, TransformTypeValue<U7>]>
                  : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8]
                    ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>, TransformTypeValue<U6>, TransformTypeValue<U7>, TransformTypeValue<U8>]>
                    : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8, infer U9]
                      ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>, TransformTypeValue<U6>, TransformTypeValue<U7>, TransformTypeValue<U8>, TransformTypeValue<U9>]>
                      : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8, infer U9, infer U10]
                        ? FormatTuple<[TransformTypeValue<U1>, TransformTypeValue<U2>, TransformTypeValue<U3>, TransformTypeValue<U4>, TransformTypeValue<U5>, TransformTypeValue<U6>, TransformTypeValue<U7>, TransformTypeValue<U8>, TransformTypeValue<U9>, TransformTypeValue<U10>]>
                        : FormatTuple<any[]>
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      )
    : never;
}

export interface ITransformType<T extends TypeDefinition> {
  type: T extends { type: infer U }
    ? (U extends StringKey<NativeTypeMap>
      ? NativeTypeMap[U]
      : (U extends CreateTypeStatement<infer U2>
        ? U2
        : never
        )
      )
    : (T extends SetDefinition<infer U>
      ? (U extends StringKey<NativeTypeMap>
        ? FormatSet<NativeTypeMap[U]>
        : TransformTypeValue<T>
        )
      : (T extends ListDefinition<infer U>
        ? (U extends StringKey<NativeTypeMap>
          ? FormatList<NativeTypeMap[U]>
          : TransformTypeValue<T>
          )
        : (T extends MapDefinition<infer K, infer V>
          ? FormatMap<K extends StringKey<NativeTypeMap> ? NativeTypeMap[K] : TransformTypeValue<K>, V extends StringKey<NativeTypeMap> ? NativeTypeMap[V] : TransformTypeValue<V>>
          : TransformTypeValue<T>
          )
        )
      )
}

export type TransformType<T extends TypeDefinition> = ITransformType<T>['type'];


export type TransformPossibleTypeValue<T> = T extends { type: StringKey<PossibleNativeTypeMap> }
  ? PossibleNativeTypeMap[T['type']]
  : IComplexTransformPossibleType<T>['type'];

export interface IComplexTransformPossibleType<T> {
  type: T extends { type: infer U }
    ? (U extends StringKey<PossibleNativeTypeMap>
      ? PossibleNativeTypeMap[U]
      : (U extends CreateTypeStatement<infer U2>
        ? (U2 extends ColumnDefinition
          ? ({ [K in StringKey<U2>]: TransformPossibleTypeValue<U2[K]> })
          : any
          )
        : never
        )
      )
    : (T extends SetDefinition
      ? this['set']
      : (T extends ListDefinition
        ? this['list']
        : (T extends MapDefinition
          ? this['map']
          : (T extends TupleDefinition
            ? this['tuple']
            : never
            )
          )
        )
      );
  // @ts-ignore
  set: T extends SetDefinition<infer U>
    ? FormatPossibleSet<TransformPossibleTypeValue<U>>
    : never;
  // @ts-ignore
  list: T extends ListDefinition<infer U>
    ? FormatPossibleList<TransformPossibleTypeValue<U>>
    : never;
  // @ts-ignore
  map: T extends MapDefinition<infer K, infer V>
    ? FormatPossibleMap<TransformPossibleTypeValue<K>, TransformPossibleTypeValue<V>>
    : never;
  // @ts-ignore
  tuple: T extends TupleDefinition<infer U>
    ? (U extends [infer U1]
      ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>]>
      : (U extends [infer U1, infer U2]
        ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>]>
        : (U extends [infer U1, infer U2, infer U3]
          ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>]>
          : (U extends [infer U1, infer U2, infer U3, infer U4]
            ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>]>
            : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5]
              ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>]>
              : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6]
                ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>, TransformPossibleTypeValue<U6>]>
                : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7]
                  ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>, TransformPossibleTypeValue<U6>, TransformPossibleTypeValue<U7>]>
                  : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8]
                    ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>, TransformPossibleTypeValue<U6>, TransformPossibleTypeValue<U7>, TransformPossibleTypeValue<U8>]>
                    : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8, infer U9]
                      ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>, TransformPossibleTypeValue<U6>, TransformPossibleTypeValue<U7>, TransformPossibleTypeValue<U8>, TransformPossibleTypeValue<U9>]>
                      : (U extends [infer U1, infer U2, infer U3, infer U4, infer U5, infer U6, infer U7, infer U8, infer U9, infer U10]
                        ? FormatPossibleTuple<[TransformPossibleTypeValue<U1>, TransformPossibleTypeValue<U2>, TransformPossibleTypeValue<U3>, TransformPossibleTypeValue<U4>, TransformPossibleTypeValue<U5>, TransformPossibleTypeValue<U6>, TransformPossibleTypeValue<U7>, TransformPossibleTypeValue<U8>, TransformPossibleTypeValue<U9>, TransformPossibleTypeValue<U10>]>
                        : FormatPossibleTuple<any[]>
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      )
    : never;
}

export interface ITransformPossibleType<T extends TypeDefinition> {
  type: T extends { type: infer U }
    ? (U extends StringKey<PossibleNativeTypeMap>
      ? PossibleNativeTypeMap[U]
      : (U extends CreateTypeStatement<infer U2>
        ? U2
        : never
        )
      )
    : (T extends SetDefinition<infer U>
      ? (U extends StringKey<PossibleNativeTypeMap>
        ? FormatPossibleSet<PossibleNativeTypeMap[U]>
        : TransformPossibleTypeValue<T>
        )
      : (T extends ListDefinition<infer U>
        ? (U extends StringKey<PossibleNativeTypeMap>
          ? FormatPossibleList<PossibleNativeTypeMap[U]>
          : TransformPossibleTypeValue<T>
          )
        : (T extends MapDefinition<infer K, infer V>
          ? FormatPossibleMap<K extends StringKey<PossibleNativeTypeMap> ? PossibleNativeTypeMap[K] : TransformPossibleTypeValue<K>, V extends StringKey<PossibleNativeTypeMap> ? PossibleNativeTypeMap[V] : TransformPossibleTypeValue<V>>
          : TransformPossibleTypeValue<T>
          )
        )
      )
}

export type TransformPossibleType<T extends TypeDefinition> = ITransformPossibleType<T>['type'];