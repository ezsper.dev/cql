import * as driver from 'cassandra-driver';
import {
  TypeDefinition,
} from './Type';
import { CreateTypeStatement } from './CreateTypeStatement';
import { Raw } from './Raw';
import {
  TransformType,
  TransformPossibleType,
} from './TransformType';

export {
  TransformType,
  TransformPossibleType,
} from './TransformType';

export interface Tuple<T> {
  elements: T;
  length: number;
  get(index: number): any;
  toString(): string;
  toJSON(): string;
  values(): T;
}

export type ColumnDefinition = { [name: string]: TypeDefinition };

export interface NativeTypeMap {
  ascii: string;
  bigint: typeof driver.types.Long;
  blob: Buffer;
  boolean: boolean;
  counter: typeof driver.types.Long;
  date: driver.types.LocalDate;
  decimal: driver.types.BigDecimal;
  double: number;
  float: number;
  inet: driver.types.InetAddress;
  int: number;
  smallint: number;
  text: string;
  time: driver.types.LocalTime;
  timestamp: Date;
  timeuuid: driver.types.TimeUuid;
  tinyint: number;
  uuid: driver.types.Uuid;
  varchar: string;
  varint: driver.types.Integer;
}

export interface PossibleNativeTypeMap {
  ascii: string;
  bigint: number | string | typeof driver.types.Long;
  blob: Buffer | string;
  boolean: boolean | string;
  counter: string | typeof driver.types.Long | number;
  date: string | driver.types.LocalDate;
  decimal: string | driver.types.BigDecimal;
  double: string | number;
  float: string | number;
  inet: string | driver.types.InetAddress;
  int: string | number;
  smallint: string | number;
  text: string;
  time: string | driver.types.LocalTime;
  timestamp: string | Date;
  timeuuid: string | driver.types.TimeUuid;
  tinyint: string | number;
  uuid: string | driver.types.Uuid;
  varchar: string | string;
  varint: string | driver.types.Integer;
}

export type FormatSet<T> = Set<T>;
export type FormatList<T> = T[];
export type FormatMap<K, V> = Map<K, V>;
export type FormatTuple<T> = Tuple<T>;

export type FormatPossibleSet<T> = Set<T> | T[];
export type FormatPossibleList<T> = T[];
export type FormatPossibleMap<K, V> = Map<K, V> | [K, V][];
export type FormatPossibleTuple<T> = Tuple<T> | T;

export type TransformColumnDefinition<C extends ColumnDefinition = ColumnDefinition> = { [K in StringKey<C>]: TransformType<C[K]> };
export type TransformPossibleColumnDefinition<C extends ColumnDefinition = ColumnDefinition> = { [K in StringKey<C>]: TransformPossibleType<C[K]> };

export type StringKey<T extends {}> = Exclude<keyof T, number | symbol>

export interface SelectionAlias<S extends {}, K extends StringKey<S>, A extends string = A> {
  select: K;
  alias: A;
}

export type Selection<S extends {}, K extends StringKey<S> = StringKey<S>, A extends string = StringKey<S>> = SelectionAlias<S, K, A> | K;
/*
const MyType = (new CreateTypeStatement())
  .columns({
    title: Type.text,
  });

const myType = Type.tuple(Type.set(Type.type(MyType)), Type.set(Type.type(MyType)));

const a: TransformType<typeof myType> = 1;

if (a.elements) {

}
*/

export function getColumnType(def: TypeDefinition): Raw {
  let query: string;
  const params: any[] = [];
  if ('type' in def) {
    if (def.type instanceof CreateTypeStatement) {
      return def.type.getColumnFamilyDef();
    }
    query = def.type;
  } else if ('set' in def) {
    const { query: typeQuery, params: typeParams } = getColumnType(def.set);
    query = `set<${typeQuery}>`;
    params.push(...typeParams);
  } else if ('list' in def) {
    const { query: typeQuery, params: typeParams } = getColumnType(def.list);
    query = `list<${typeQuery}>`;
    params.push(...typeParams);
  } else if ('map' in def) {
    const [key, value] = def.map;
    const { query: keyQuery, params: keyParams } = getColumnType(key);
    const { query: valueQuery, params: valueParams } = getColumnType(value);
    query = `map<${keyQuery}, ${valueQuery}>`;
    params.push(...keyParams, ...valueParams);
  } else if ('tuple' in def) {
    query = `<${(<any[]>def.tuple).map(typeDef => {
      const { query: typeQuery, params: typeParams } = getColumnType(typeDef);
      params.push(...typeParams);
      return typeQuery;
    }).join(', ')}>`;
  } else {
    throw new Error();
  }
  if ('frozen' in def && def.frozen) {
    query = `frozen<${query}>`;
  }
  return new Raw(query, params);
}