import { Builder } from '..';

describe('CreateKeyspaceStatement', () => {
  const builder = new Builder();

  it('NetworkTopologyStrategy', () => {
    const keyspace = builder.createKeyspace('global')
      .withOptions({
        replication: {
          class: 'NetworkTopologyStrategy',
          replicationFactor: {
            'dc_1': 1,
            'dc_2': 5,
          },
        },
      });
    const { query, params } = keyspace.build();
    expect(query).toBe(
`CREATE KEYSPACE "global" WITH REPLICATION = { 'class': 'NetworkTopologyStrategy', 'dc_1': 1, 'dc_2': 5 }`,
    );
    expect(params.length).toBe(0);
  });

  it('SimpleStrategy', () => {
    const keyspace = builder.createKeyspace('account')
      .ifNotExists()
      .withOptions({
        replication: {
          class: 'SimpleStrategy',
          replicationFactor: 1,
        },
        durableWrites: true,
      });
    const { query, params } = keyspace.build();
    expect(query).toBe(
`CREATE KEYSPACE IF NOT EXISTS "account" WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': 1 } AND DURABLE_WRITES = true`,
    );
    expect(params.length).toBe(0);
  });

});
