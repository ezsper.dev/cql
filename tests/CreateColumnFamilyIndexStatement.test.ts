import { Builder, Type } from '..';

describe('CreateColumnFamilyStatement', () => {
  const builder = new Builder();

  const columnFamily = builder
    .createColumnFamily('account', 'Post')
    .columns({
      id: Type.text,
      accountId: Type.text,
      title: Type.text,
      tags: Type.set(Type.text),
      authorId: Type.text,
    })
    .partitionKeys('id');

  it('Basic', () => {
    const index = columnFamily.createIndex('tags');
    const { query, params } = index.build();
    expect(query).toBe(
  `CREATE INDEX ON "account"."Post" ( "tags" )`,
    );
    expect(params.length).toBe(0);
  });

});
