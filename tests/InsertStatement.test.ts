import { Builder } from '..';

describe('Insert statements', () => {
  const builder = new Builder();

  it('Basic', () => {
    const { query, params } = builder
      .insert()
      .values({
        id: '1423ef10-0801-4179-ba70-d8e5b3a2effc',
        displayName: 'John Doe'
      })
      .into('User')
      .build();
    expect(query).toBe(
`INSERT INTO "User" ("id", "displayName") VALUES (?, ?)`,
    );
    expect(params.length).toBe(2);
    expect(params[0]).toBe('1423ef10-0801-4179-ba70-d8e5b3a2effc');
    expect(params[1]).toBe('John Doe');
  });

});
