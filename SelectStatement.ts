import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import {
  StringKey,
  Selection,
  TransformColumnDefinition,
  TransformPossibleColumnDefinition,
} from './ColumnDefinition';

export class SelectStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, P extends {} = {}> extends WhereStatement<S, P> {

  protected selectionDefs: Raw[] = [];
  protected keyspaceDef: Raw;
  protected columnFamilyDef: Raw;
  protected orderByDefs: Raw[] = [];
  protected limitDef?: number;
  protected distinctDef = false;
  protected allowFilteringDef = false;

  distinct(value = true): this {
    this.distinctDef = value;
    return this;
  }

  count(): this {
    this.selectionDefs.push(new Raw('COUNT(*)'));
    return this;
  }

  select(selection: '*'): SelectStatement<R, S, R>;
  select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, P & (SS extends Selection<R, infer K, infer A> ? { [K2 in A]: R[K] } : never)>;
  select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, P & Pick<R, K>>;
  select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, P & { [K in A]: R[K] }>;
  select(...args: any[]): any {
    if (args.length === 2 || typeof args[0] === 'string') {
      const [selection, alias] = args;
      let def: Raw;
      if (selection instanceof Raw) {
        if (alias == null) {
          def = selection;
        } else {
          def = new Raw(`${selection.query} AS "${alias}"`, selection.params);
        }
      } else {
        const params: any[] = [];
        let query = selection === '*' ? selection : `"${selection}"`;
        if (alias != null) {
          query = `${query} AS "${alias}"`;
        }
        def = new Raw(query, params);
      }
      this.selectionDefs.push(def);
      return this;
    }
    if (Array.isArray(args[0])) {
      for (const selection of args[0]) {
        if (selection instanceof Raw || typeof selection === 'string') {
          this.select(<any>selection);
        } else {
          this.select(<any>[selection]);
        }
      }
    }
    return this;
  }

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  from(name: string | Raw): this;
  from(keyspace: string | Raw, name: string | Raw): this;
  from(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  orderBy<K extends StringKey<R>>(name: K, order: 'ASC' | 'DESC') {
    let def: Raw;
    def = new Raw(`"${name}" ${order}`);
    this.orderByDefs.push(def);
    return this;
  }

  limit(value: number) {
    if (this.limitDef != null) {
      throw new Error(`The LIMIT clause was already defined`);
    }
    this.limitDef = value;
    return this;
  }

  allowFiltering(value = true) {
    this.allowFilteringDef = value;
    return this;
  }

  protected validate() {
    if (this.selectionDefs.length === 0) {
      throw new Error(`No SELECT expression was defined`);
    }
    if (this.columnFamilyDef == null) {
      throw new Error(`No FROM column family was defined`);
    }
  }

  protected getParams() {
    const params: any[] = [];
    for (const def of this.selectionDefs) {
      params.push(...def.params);
    }
    params.push(...this.columnFamilyDef.params);
    params.push(...this.getWhereParams());
    for (const def of this.orderByDefs) {
      params.push(...def.params);
    }
    return params;
  }

  protected getQuery(): string {
    let query = `SELECT ${this.distinctDef ? 'DISTINCT ' : ''}`;

    query += `${this.selectionDefs.join(', ')}`;

    query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef}`;

    query += this.getWhereQuery();

    if (this.orderByDefs.length > 0) {
      query += ` ORDER BY ${this.orderByDefs.join(', ')}`;
    }

    if (this.limitDef != null) {
      query += ` LIMIT ${this.limitDef}`;
    }

    if (this.allowFilteringDef) {
      query += ' ALLOW FILTERING'
    }

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}