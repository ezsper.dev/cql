/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../internal/utils/exec';

const distPath = pathResolve(appRootDir.get(), 'dist');
// First clear the build output dir.
fs.removeSync(distPath);
// Build typescript
exec('tsc');

fs.copySync(
  distPath,
  pathResolve(appRootDir.get(), '.dist-lite'),
);

fs.moveSync(
  pathResolve(appRootDir.get(), '.dist-lite'),
  pathResolve(distPath, 'lite'),
);

fs.copySync(
  pathResolve(distPath, 'lite'),
  pathResolve(distPath, 'simple'),
);

const specs = [
  ['.', 'Complex'],
  ['lite', 'Lite'],
  ['simple', 'Simple'],
];

for (const [dir, prefix] of specs) {
  fs.removeSync(pathResolve(distPath, `${dir}/TransformType.d.ts`));
  fs.removeSync(pathResolve(distPath, `${dir}/TransformType.js`));
  fs.removeSync(pathResolve(distPath, `${dir}/TransformType.js.map`));
  for (const spec of specs) {
    if (spec[1] !== prefix) {
      fs.removeSync(pathResolve(distPath, `${dir}/${spec[1]}TransformType.d.ts`));
      fs.removeSync(pathResolve(distPath, `${dir}/${spec[1]}TransformType.js`));
      fs.removeSync(pathResolve(distPath, `${dir}/${spec[1]}TransformType.js.map`));
    }
  }
  fs.moveSync(
    pathResolve(distPath, `${dir}/${prefix}TransformType.d.ts`),
    pathResolve(distPath, `${dir}/TransformType.d.ts`),
  );
  fs.moveSync(
    pathResolve(distPath, `${dir}/${prefix}TransformType.js`),
    pathResolve(distPath, `${dir}/TransformType.js`),
  );
  fs.moveSync(
    pathResolve(distPath, `${dir}/${prefix}TransformType.js.map`),
    pathResolve(distPath, `${dir}/TransformType.js.map`),
  );
}

// Copy documents
fs.copySync(
  pathResolve(appRootDir.get(), 'README.md'),
  pathResolve(distPath, 'README.md'),
);
// Clear our package json for release
const packageJson = require('../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;

fs.writeFileSync(
  pathResolve(distPath, './package.json'),
  JSON.stringify(packageJson, null, 2),
);

fs.removeSync(pathResolve(distPath, 'internal'));

fs.copySync(
  pathResolve(appRootDir.get(), 'LICENSE'),
  pathResolve(distPath, 'LICENSE'),
);
