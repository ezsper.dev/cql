"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const Raw_1 = require("./Raw");
class CreateColumnFamilyIndexStatement extends BaseStatement_1.BaseStatement {
    ifNotExists(value = true) {
        this.ifNotExistsDef = value;
        return this;
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    as(name) {
        this.secondaryIndexDef = name instanceof Raw_1.Raw ? name : new Raw_1.Raw(`${name}`);
        return this;
    }
    on(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    column(name, type) {
        const params = [];
        if (name instanceof Raw_1.Raw) {
            params.push(...name.params);
        }
        if (type == null) {
            this.columnDef = new Raw_1.Raw(`"${name}"`, params);
        }
        else {
            let column;
            if (name instanceof Raw_1.Raw) {
                column = name.query;
            }
            else {
                column = name;
            }
            this.columnDef = new Raw_1.Raw(`${type}(${column})`, params);
        }
        return this;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error('The ON name was not defined');
        }
        if (this.columnDef == null) {
            throw new Error('The COLUMN name was not defined');
        }
    }
    getParams() {
        const params = [];
        if (this.secondaryIndexDef != null) {
            params.push(...this.secondaryIndexDef.params);
        }
        if (this.keyspaceDef != null) {
            params.push(...this.keyspaceDef.params);
        }
        params.push(...this.columnFamilyDef.params);
        params.push(...this.columnDef.params);
        return params;
    }
    getQuery() {
        let query = `CREATE INDEX ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}`;
        query += `${typeof this.secondaryIndexDef !== 'undefined' ? `${this.secondaryIndexDef} ` : ''}ON `;
        query += `${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}`;
        query += ` ( ${this.columnDef} )`;
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.CreateColumnFamilyIndexStatement = CreateColumnFamilyIndexStatement;
//# sourceMappingURL=CreateColumnFamilyIndexStatement.js.map