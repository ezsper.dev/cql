# Cassandra CQL Builder

This is a personal CQL builder made on TypeScript.

### Key aspects

1. Agnostic, not related to any client driver
2. Focused exclusively on building queries
3. Builder with type definitions

## Usage

```
import * as cql from '@ezsper/cql';

const accountKeyspace = cql
  .createKeyspace('account')
  .withOptions({
    replication: {
      class: 'SimpleStrategy',
      replicationFactor: 1,
    },
  });

const Group = accountKeyspace
  .createColumnFamily('Group')
  .columns({
    id: cql.type.text,
    accountId: cql.type.text,
    displayName: cql.type.text,
  })
  .partitionKeys('accountId', 'id');

const GroupOrderByDisplayNameView = Group
  .createMaterializedView('GroupOrderByDisplayNameView')
  .columns('id', 'displayName', 'accountId')
  .partitionKeys('accountId')
  .clusteringKeys('displayName', 'id')
  .withClusteringOrder(['displayName', 'ASC'], ['id', 'ASC']);

const { query, params } = Group
  .select('*')
  .whereEquals('id', 'foo')
  .build();

expect(query).toBe(`SELECT * FROM "account"."Group" WHERE "id" = ?`);
expect(params.length).toBe(1);
expect(params[0]).toBe('foo');
```
