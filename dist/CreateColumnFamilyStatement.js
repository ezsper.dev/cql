"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const SelectStatement_1 = require("./SelectStatement");
const InsertStatement_1 = require("./InsertStatement");
const UpdateStatement_1 = require("./UpdateStatement");
const DeleteStatement_1 = require("./DeleteStatement");
const CreateMaterializedViewStatement_1 = require("./CreateMaterializedViewStatement");
const CreateColumnFamilyIndexStatement_1 = require("./CreateColumnFamilyIndexStatement");
const CreateKeyspaceStatement_1 = require("./CreateKeyspaceStatement");
const Raw_1 = require("./Raw");
const ColumnDefinition_1 = require("./ColumnDefinition");
class CreateColumnFamilyStatement extends BaseStatement_1.BaseStatement {
    constructor() {
        super(...arguments);
        this.partitionKeyDefs = [];
        this.clusteringKeyDefs = [];
        this.clusteringOrderDefs = [];
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    as(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    ifNotExists(value = true) {
        this.ifNotExistsDef = value;
        return this;
    }
    columns(columns) {
        this.columnDefs = Object.assign({}, this.columnDefs, columns);
        return this;
    }
    partitionKeys(...columns) {
        this.partitionKeyDefs = columns;
        return this;
    }
    clusteringKeys(...columns) {
        this.clusteringKeyDefs = columns;
        return this;
    }
    nullable(...columns) {
        this.nullableColumns = columns;
        return this;
    }
    withOptions(options) {
        this.optionsDef = options;
        return this;
    }
    withClusteringOrder(...clusteringOrders) {
        this.clusteringOrderDefs = clusteringOrders;
        return this;
    }
    createMaterializedView(...args) {
        const statement = new CreateMaterializedViewStatement_1.CreateMaterializedViewStatement(this.builder);
        if (args.length === 2) {
            statement.as(args[0], args[1]);
        }
        else {
            if (typeof this.keyspaceDef !== 'undefined') {
                statement.keyspace(this.keyspaceDef);
            }
            statement.as(args[0]);
        }
        statement.from(this.columnFamilyDef);
        return statement;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error('The TABLE name not defined');
        }
    }
    getParams() {
        const params = [];
        params.push(...this.columnFamilyDef.params);
        for (const column in this.columnDefs) {
            const def = this.columnDefs[column];
            if (def instanceof Raw_1.Raw) {
                params.push(...def.params);
            }
        }
        return params;
    }
    getQuery() {
        let query = `CREATE TABLE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef} (`;
        const columns = Object.keys(this.columnDefs);
        query += columns.map(column => {
            const def = this.columnDefs[column];
            const { query, params } = ColumnDefinition_1.getColumnType(def);
            return new Raw_1.Raw(`"${column}" ${query}`, params);
        }).join(', ');
        const primaryKey = [];
        if (this.partitionKeyDefs.length == 1) {
            primaryKey.push(...this.partitionKeyDefs);
        }
        else {
            primaryKey.push(this.partitionKeyDefs);
        }
        primaryKey.push(...this.clusteringKeyDefs);
        query += `, PRIMARY KEY (${primaryKey
            .map(column => Array.isArray(column)
            ? `(${column.map(partition => `"${partition}"`).join(', ')})`
            : `"${column}"`).join(', ')})`;
        query += ')';
        const withDefs = [];
        if (this.optionsDef != null) {
            withDefs.push(...CreateKeyspaceStatement_1.buildOptions(this.optionsDef));
        }
        if (this.clusteringOrderDefs.length) {
            withDefs.push(new Raw_1.Raw(`CLUSTERING ORDER BY (${this.clusteringOrderDefs.map(([column, order]) => `"${column}" ${order}`).join(', ')})`));
        }
        if (withDefs.length) {
            query += ` WITH ${withDefs.join(' AND ')}`;
        }
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
    isModel(data) {
        return true;
    }
    isInsertInput(data) {
        return true;
    }
    isUpdateInput(data) {
        return true;
    }
    createIndex(...args) {
        const statement = new CreateColumnFamilyIndexStatement_1.CreateColumnFamilyIndexStatement(this.builder);
        statement.keyspace(this.keyspaceDef);
        statement.on(this.columnFamilyDef);
        if (Array.isArray(args[0])) {
            statement.column(args[0][0], args[0][1]);
        }
        else {
            statement.column(args[0]);
        }
        if (args[1] != null) {
            statement.as(args[1]);
        }
        return statement;
    }
    select(...args) {
        const statement = new SelectStatement_1.SelectStatement(this.builder);
        if (args.length === 2) {
            statement.select(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.select(args[0]);
        }
        if (typeof this.keyspaceDef !== 'undefined') {
            statement.keyspace(this.keyspaceDef);
        }
        statement.from(this.columnFamilyDef);
        return statement;
    }
    insert(...args) {
        const statement = new InsertStatement_1.InsertStatement(this.builder);
        if (args.length > 0) {
            if (args.length > 1) {
                statement.value(...args);
            }
            else {
                statement.values(...args);
            }
        }
        if (typeof this.keyspaceDef !== 'undefined') {
            statement.keyspace(this.keyspaceDef);
        }
        statement.into(this.columnFamilyDef);
        return statement;
    }
    update(...args) {
        const statement = new UpdateStatement_1.UpdateStatement(this.builder);
        if (typeof this.keyspaceDef !== 'undefined') {
            statement.keyspace(this.keyspaceDef);
        }
        statement.on(this.columnFamilyDef);
        if (args.length > 0) {
            statement.set(...args);
        }
        return statement;
    }
    delete(...args) {
        const statement = new DeleteStatement_1.DeleteStatement(this.builder);
        if (args.length > 0) {
            statement.columns(...args);
        }
        if (typeof this.keyspaceDef !== 'undefined') {
            statement.keyspace(this.keyspaceDef);
        }
        statement.from(this.columnFamilyDef);
        return statement;
    }
}
CreateColumnFamilyStatement.Update = UpdateStatement_1.UpdateStatement;
CreateColumnFamilyStatement.Insert = InsertStatement_1.InsertStatement;
CreateColumnFamilyStatement.Delete = DeleteStatement_1.DeleteStatement;
CreateColumnFamilyStatement.CreateColumnFamilyIndex = CreateColumnFamilyIndexStatement_1.CreateColumnFamilyIndexStatement;
CreateColumnFamilyStatement.CreateMaterializedView = CreateMaterializedViewStatement_1.CreateMaterializedViewStatement;
exports.CreateColumnFamilyStatement = CreateColumnFamilyStatement;
//# sourceMappingURL=CreateColumnFamilyStatement.js.map