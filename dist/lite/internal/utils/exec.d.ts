/// <reference types="node" />
import { ExecSyncOptions } from 'child_process';
export declare function exec(command: string, options?: ExecSyncOptions): void;
