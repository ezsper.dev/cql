"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const Raw_1 = require("./Raw");
const unix_1 = require("./utils/unix");
class InsertStatement extends BaseStatement_1.BaseStatement {
    constructor() {
        super(...arguments);
        this.columnDefs = [];
        this.valueDefs = [];
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    into(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    values(data) {
        for (const column in data) {
            if (data.hasOwnProperty(column)) {
                this.columnDefs.push(new Raw_1.Raw(`"${column}"`));
                this.valueDefs.push(data[column] instanceof Raw_1.Raw ? data[column] : new Raw_1.Raw(`?`, [data[column]]));
            }
        }
        return this;
    }
    value(column, value) {
        this.columnDefs.push(new Raw_1.Raw(`"${column}"`));
        this.valueDefs.push(new Raw_1.Raw(`?`, [value]));
        return this;
    }
    usingTTL(value) {
        this.ttl = unix_1.unix(value, true);
        return this;
    }
    usingTimestamp(value) {
        this.timestamp = unix_1.unix(value);
        return this;
    }
    validate() {
        if (this.columnDefs.length === 0) {
            throw new Error(`No COLUMNS were defined`);
        }
        if (this.valueDefs.length === 0) {
            throw new Error(`No VALUES were defined`);
        }
        if (this.columnDefs.length !== this.valueDefs.length) {
            throw new Error(`The columns and values must match on length`);
        }
    }
    getParams() {
        const params = [];
        params.push(...this.columnFamilyDef.params);
        for (const def of this.columnDefs) {
            params.push(...def.params);
        }
        for (const def of this.valueDefs) {
            params.push(...def.params);
        }
        return params;
    }
    getQuery() {
        const usingDefs = [];
        if (this.ttl != null) {
            usingDefs.push(`TTL ${this.ttl}`);
        }
        if (this.timestamp != null) {
            usingDefs.push(`TIMESTAMP ${this.timestamp}`);
        }
        let query = `INSERT INTO ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}`;
        query += ` (${this.columnDefs.join(', ')})`;
        query += ` VALUES (${this.valueDefs.join(', ')})`;
        if (usingDefs.length > 0) {
            query += ` USING ${usingDefs.join(' AND ')}`;
        }
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.InsertStatement = InsertStatement;
//# sourceMappingURL=InsertStatement.js.map