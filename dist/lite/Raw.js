"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Raw {
    constructor(query, params = []) {
        this.query = query;
        this.params = params;
    }
    toString() {
        return this.query;
    }
}
exports.Raw = Raw;
function raw(strings, ...values) {
    const params = [];
    let query = String.raw(strings, ...values.map((value) => {
        if (value instanceof Raw) {
            params.push(...value.params);
            return value.query;
        }
        params.push(value);
        return '?';
    }));
    return new Raw(query, params);
}
exports.raw = raw;
//# sourceMappingURL=Raw.js.map