"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WhereStatement_1 = require("./WhereStatement");
const Raw_1 = require("./Raw");
class DeleteStatement extends WhereStatement_1.WhereStatement {
    constructor() {
        super(...arguments);
        this.columnDefs = [];
    }
    columns(columns) {
        for (const column of columns) {
            this.column(column);
        }
        return this;
    }
    column(name) {
        this.columnDefs.push(new Raw_1.Raw(name));
        return this;
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    from(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error(`No FROM column family was defined`);
        }
    }
    getParams() {
        const params = [];
        for (const def of this.columnDefs) {
            params.push(...def.params);
        }
        params.push(...this.columnFamilyDef.params);
        params.push(...this.getWhereParams());
        return params;
    }
    getQuery() {
        let query = `DELETE`;
        if (this.columnDefs.length) {
            query += ` ${this.columnDefs.join(', ')}`;
        }
        query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}`;
        query += this.getWhereQuery();
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.DeleteStatement = DeleteStatement;
//# sourceMappingURL=DeleteStatement.js.map