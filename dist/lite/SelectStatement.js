"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WhereStatement_1 = require("./WhereStatement");
const Raw_1 = require("./Raw");
class SelectStatement extends WhereStatement_1.WhereStatement {
    constructor() {
        super(...arguments);
        this.selectionDefs = [];
        this.orderByDefs = [];
        this.distinctDef = false;
        this.allowFilteringDef = false;
    }
    distinct(value = true) {
        this.distinctDef = value;
        return this;
    }
    count() {
        this.selectionDefs.push(new Raw_1.Raw('COUNT(*)'));
        return this;
    }
    select(...args) {
        if (args.length === 2 || typeof args[0] === 'string') {
            const [selection, alias] = args;
            let def;
            if (selection instanceof Raw_1.Raw) {
                if (alias == null) {
                    def = selection;
                }
                else {
                    def = new Raw_1.Raw(`${selection.query} AS "${alias}"`, selection.params);
                }
            }
            else {
                const params = [];
                let query = selection === '*' ? selection : `"${selection}"`;
                if (alias != null) {
                    query = `${query} AS "${alias}"`;
                }
                def = new Raw_1.Raw(query, params);
            }
            this.selectionDefs.push(def);
            return this;
        }
        if (Array.isArray(args[0])) {
            for (const selection of args[0]) {
                if (selection instanceof Raw_1.Raw || typeof selection === 'string') {
                    this.select(selection);
                }
                else {
                    this.select([selection]);
                }
            }
        }
        return this;
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    from(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    orderBy(name, order) {
        let def;
        def = new Raw_1.Raw(`"${name}" ${order}`);
        this.orderByDefs.push(def);
        return this;
    }
    limit(value) {
        if (this.limitDef != null) {
            throw new Error(`The LIMIT clause was already defined`);
        }
        this.limitDef = value;
        return this;
    }
    allowFiltering(value = true) {
        this.allowFilteringDef = value;
        return this;
    }
    validate() {
        if (this.selectionDefs.length === 0) {
            throw new Error(`No SELECT expression was defined`);
        }
        if (this.columnFamilyDef == null) {
            throw new Error(`No FROM column family was defined`);
        }
    }
    getParams() {
        const params = [];
        for (const def of this.selectionDefs) {
            params.push(...def.params);
        }
        params.push(...this.columnFamilyDef.params);
        params.push(...this.getWhereParams());
        for (const def of this.orderByDefs) {
            params.push(...def.params);
        }
        return params;
    }
    getQuery() {
        let query = `SELECT ${this.distinctDef ? 'DISTINCT ' : ''}`;
        query += `${this.selectionDefs.join(', ')}`;
        query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}`;
        query += this.getWhereQuery();
        if (this.orderByDefs.length > 0) {
            query += ` ORDER BY ${this.orderByDefs.join(', ')}`;
        }
        if (this.limitDef != null) {
            query += ` LIMIT ${this.limitDef}`;
        }
        if (this.allowFilteringDef) {
            query += ' ALLOW FILTERING';
        }
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.SelectStatement = SelectStatement;
//# sourceMappingURL=SelectStatement.js.map