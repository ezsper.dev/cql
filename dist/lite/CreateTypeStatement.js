"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const Raw_1 = require("./Raw");
const ColumnDefinition_1 = require("./ColumnDefinition");
class CreateTypeStatement extends BaseStatement_1.BaseStatement {
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    as(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    ifNotExists(value = true) {
        this.ifNotExistsDef = value;
        return this;
    }
    columns(columns) {
        this.columnDefs = Object.assign({}, this.columnDefs, columns);
        return this;
    }
    getColumnFamilyDef() {
        return this.columnFamilyDef;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error('The TABLE name not defined');
        }
    }
    getParams() {
        const params = [];
        params.push(...this.columnFamilyDef.params);
        for (const column in this.columnDefs) {
            const def = this.columnDefs[column];
            if (def instanceof Raw_1.Raw) {
                params.push(...def.params);
            }
        }
        return params;
    }
    getQuery() {
        let query = `CREATE TYPE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef} (`;
        const columns = Object.keys(this.columnDefs);
        query += columns.map(column => {
            const def = this.columnDefs[column];
            const { query, params } = ColumnDefinition_1.getColumnType(def);
            return new Raw_1.Raw(`"${column}" ${query}`, params);
        }).join(', ');
        query += ')';
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.CreateTypeStatement = CreateTypeStatement;
//# sourceMappingURL=CreateTypeStatement.js.map