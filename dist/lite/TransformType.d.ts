import { TypeDefinition } from './Type';
export declare type TransformType<T extends TypeDefinition> = any;
export declare type TransformPossibleType<T extends TypeDefinition> = any;
