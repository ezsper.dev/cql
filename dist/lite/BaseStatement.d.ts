import * as driver from 'cassandra-driver';
import { Builder } from './Builder';
import { Raw } from './Raw';
import { StringKey } from './ColumnDefinition';
export declare type Row<T> = {
    get<K extends StringKey<T>>(column: K): {
        [K2 in K]: T[K2];
    };
    values(): T[];
    keys(): keyof T;
    forEach(callback: driver.Callback): void;
} & {
    [K in StringKey<T>]: T[K];
};
export interface ResultSet<T> {
    info: {
        queriedHost: driver.Host;
        triedHosts: {
            [key: string]: string;
        };
        speculativeExecutions: number;
        achievedConsistency: driver.types.consistencies;
        traceId: driver.types.Uuid;
        warnings: string[];
        customPayload: any;
    };
    rows: Row<T>[];
    rowLength: number;
    columns: {
        [K in StringKey<T>]: string;
    }[];
    pageState: string;
    nextPage: Function;
    first(): Row<T> | null;
    getPageState(): string;
    getColumns(): {
        [K in StringKey<T>]: string;
    }[];
    wasApplied(): boolean;
    [Symbol.iterator](): Iterator<Row<T>>;
}
export interface EachRowIterator<T> {
    (index: number, row: Row<T>): any;
}
export interface EachRowCallback<T> {
    (error?: Error): void;
}
export declare abstract class BaseStatement<P extends {
    [name: string]: any;
} = {}> {
    protected builder: Builder;
    constructor(builder: Builder);
    build(): Raw;
    execute(client: driver.Client, options?: driver.QueryOptions): Promise<ResultSet<P>>;
    eachRown(client: driver.Client, options?: driver.QueryOptions, iterator?: EachRowIterator<P>, callback?: EachRowCallback<P>): Promise<void>;
}
