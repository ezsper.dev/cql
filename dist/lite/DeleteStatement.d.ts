import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import { StringKey, TransformPossibleColumnDefinition } from './ColumnDefinition';
export declare class DeleteStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends WhereStatement<S> {
    protected columnDefs: Raw[];
    protected keyspaceDef: Raw;
    protected columnFamilyDef: Raw;
    columns<K extends StringKey<S>>(columns: K[]): this;
    column<K extends StringKey<S>>(name: K): this;
    keyspace(name: string | Raw): this;
    from(name: string | Raw): this;
    from(keyspace: string | Raw, name: string | Raw): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
