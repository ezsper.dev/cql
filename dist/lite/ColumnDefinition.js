"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CreateTypeStatement_1 = require("./CreateTypeStatement");
const Raw_1 = require("./Raw");
/*
const MyType = (new CreateTypeStatement())
  .columns({
    title: Type.text,
  });

const myType = Type.tuple(Type.set(Type.type(MyType)), Type.set(Type.type(MyType)));

const a: TransformType<typeof myType> = 1;

if (a.elements) {

}
*/
function getColumnType(def) {
    let query;
    const params = [];
    if ('type' in def) {
        if (def.type instanceof CreateTypeStatement_1.CreateTypeStatement) {
            return def.type.getColumnFamilyDef();
        }
        query = def.type;
    }
    else if ('set' in def) {
        const { query: typeQuery, params: typeParams } = getColumnType(def.set);
        query = `set<${typeQuery}>`;
        params.push(...typeParams);
    }
    else if ('list' in def) {
        const { query: typeQuery, params: typeParams } = getColumnType(def.list);
        query = `list<${typeQuery}>`;
        params.push(...typeParams);
    }
    else if ('map' in def) {
        const [key, value] = def.map;
        const { query: keyQuery, params: keyParams } = getColumnType(key);
        const { query: valueQuery, params: valueParams } = getColumnType(value);
        query = `map<${keyQuery}, ${valueQuery}>`;
        params.push(...keyParams, ...valueParams);
    }
    else if ('tuple' in def) {
        query = `<${def.tuple.map(typeDef => {
            const { query: typeQuery, params: typeParams } = getColumnType(typeDef);
            params.push(...typeParams);
            return typeQuery;
        }).join(', ')}>`;
    }
    else {
        throw new Error();
    }
    if ('frozen' in def && def.frozen) {
        query = `frozen<${query}>`;
    }
    return new Raw_1.Raw(query, params);
}
exports.getColumnType = getColumnType;
//# sourceMappingURL=ColumnDefinition.js.map