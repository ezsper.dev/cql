"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WhereStatement_1 = require("./WhereStatement");
const Raw_1 = require("./Raw");
const unix_1 = require("./utils/unix");
class UpdateStatement extends WhereStatement_1.WhereStatement {
    constructor() {
        super(...arguments);
        this.setClauseDefs = [];
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    on(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[0];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.columnFamilyDef = args[1];
            }
            else {
                this.columnFamilyDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    setValue(selection, operator, value) {
        const params = [];
        let query;
        if (Array.isArray(selection)) {
            query = `"${selection[0]}"[?]`;
            params.push(selection[1], selection[1]);
        }
        else {
            query = `"${selection}"`;
        }
        params.push(value);
        if (operator === '=') {
            query = `${query} = ?`;
        }
        else {
            query = `${query} = ${query} ${operator} ?`;
        }
        this.setClauseDefs.push(new Raw_1.Raw(query, params));
        return this;
    }
    set(...args) {
        if (args.length === 1) {
            if (Array.isArray(args[0])) {
                for (const [selection, value] of args[0]) {
                    this.set(selection, value);
                }
            }
            else if (typeof args[0] === 'object' && args[0] != null) {
                for (const column in args[0]) {
                    this.set(column, args[0][column]);
                }
            }
            return this;
        }
        return this.setValue(args[0], '=', args[1]);
    }
    add(selection, value) {
        return this.setValue(selection, '+', value);
    }
    remove(selection, value) {
        return this.setValue(selection, '-', value);
    }
    increment(selection, value) {
        return this.setValue(selection, '+', value);
    }
    decrement(selection, value) {
        return this.setValue(selection, '-', value);
    }
    usingTTL(value) {
        this.ttl = unix_1.unix(value, true);
        return this;
    }
    usingTimestamp(value) {
        this.timestamp = unix_1.unix(value);
        return this;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error(`No UPDATE column family was defined`);
        }
        if (this.setClauseDefs.length === 0) {
            throw new Error('No SET clause was applied');
        }
        return this;
    }
    getParams() {
        const params = [];
        params.push(...this.columnFamilyDef.params);
        for (const def of this.setClauseDefs) {
            params.push(...def.params);
        }
        params.push(...this.getWhereParams());
        return params;
    }
    getQuery() {
        const usingDefs = [];
        if (this.ttl != null) {
            usingDefs.push(`TTL ${this.ttl}`);
        }
        if (this.timestamp != null) {
            usingDefs.push(`TIMESTAMP ${this.timestamp}`);
        }
        let query = `UPDATE ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}${usingDefs.length > 0 ? ` USING ${usingDefs.join(' AND ')}` : ''} SET ${this.setClauseDefs.join(', ')}`;
        query += this.getWhereQuery();
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
exports.UpdateStatement = UpdateStatement;
//# sourceMappingURL=UpdateStatement.js.map