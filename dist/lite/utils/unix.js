"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function unix(value, interval = false) {
    let ms;
    const now = (new Date()).getTime();
    if (typeof value === 'string') {
        ms = require('ms')(value);
    }
    else if (value instanceof Date) {
        ms = value.getTime() - now;
    }
    else {
        return value;
    }
    const unix = Math.round(ms / 1000);
    return interval ? unix : (Math.round(now / 1000) + unix);
}
exports.unix = unix;
//# sourceMappingURL=unix.js.map