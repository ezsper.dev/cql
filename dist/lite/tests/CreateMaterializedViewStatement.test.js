"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
describe('CreateColumnFamilyStatement', () => {
    const builder = new __1.Builder();
    it('With Basic Options', () => {
        const columnFamily = builder
            .createMaterializedView('global', 'UserByName')
            .columns('id', 'displayName', 'accountId')
            .from('User')
            .partitionKeys('accountId')
            .clusteringKeys('displayName', 'id')
            .withClusteringOrder(['displayName', 'ASC'], ['id', 'ASC'])
            .withOptions({
            comment: '',
        });
        const { query, params } = columnFamily.build();
        expect(query).toBe(`CREATE MATERIALIZED VIEW "global"."UserByName" AS SELECT "id", "displayName", "accountId" FROM "global"."User" WHERE "accountId" IS NOT NULL AND "displayName" IS NOT NULL AND "id" IS NOT NULL PRIMARY KEY ("accountId", "displayName", "id") WITH COMMENT = '' AND CLUSTERING ORDER BY ("displayName" ASC, "id" ASC)`);
        expect(params.length).toBe(0);
    });
});
//# sourceMappingURL=CreateMaterializedViewStatement.test.js.map