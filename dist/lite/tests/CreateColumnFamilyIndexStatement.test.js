"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
describe('CreateColumnFamilyStatement', () => {
    const builder = new __1.Builder();
    const columnFamily = builder
        .createColumnFamily('account', 'Post')
        .columns({
        id: __1.Type.text,
        accountId: __1.Type.text,
        title: __1.Type.text,
        tags: __1.Type.set(__1.Type.text),
        authorId: __1.Type.text,
    })
        .partitionKeys('id');
    it('Basic', () => {
        const index = columnFamily.createIndex('tags');
        const { query, params } = index.build();
        expect(query).toBe(`CREATE INDEX ON "account"."Post" ( "tags" )`);
        expect(params.length).toBe(0);
    });
});
//# sourceMappingURL=CreateColumnFamilyIndexStatement.test.js.map