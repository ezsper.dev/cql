"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
describe('Select statements', () => {
    const builder = new __1.Builder();
    const accountKeyspace = builder
        .createKeyspace('account')
        .withOptions({
        replication: {
            class: 'SimpleStrategy',
            replicationFactor: 1,
        },
    });
    const Group = accountKeyspace
        .createColumnFamily('Group')
        .columns({
        id: __1.Type.text,
        accountId: __1.Type.int,
        displayName: __1.Type.text,
        createdAt: __1.Type.timestamp,
    })
        .partitionKeys('accountId', 'id');
    const GroupOrderByDisplayNameView = Group
        .createMaterializedView('GroupOrderByDisplayNameView')
        .columns('id', 'displayName', 'accountId')
        .partitionKeys('accountId')
        .clusteringKeys('displayName', 'id')
        .withClusteringOrder(['displayName', 'ASC'], ['id', 'ASC']);
    it('Basic', () => {
        const { query, params } = Group
            .select('*')
            .whereEquals('id', 'foo')
            .build();
        expect(query).toBe(`SELECT * FROM "account"."Group" WHERE "id" = ?`);
        expect(params.length).toBe(1);
        expect(params[0]).toBe('foo');
    });
    it('Keyspace defined', () => {
        const { query, params } = GroupOrderByDisplayNameView
            .select(['id', 'displayName'])
            .build();
        expect(query).toBe(`SELECT "id", "displayName" FROM "account"."GroupOrderByDisplayNameView"`);
        expect(params.length).toBe(0);
    });
    it('Timestamp', () => {
        const statement = Group
            .select(['accountId', 'id'])
            .where('createdAt', '>', (new Date()))
            .allowFiltering();
        const { query, params } = statement.build();
        expect(query).toBe(`SELECT "accountId", "id" FROM "account"."Group" WHERE "createdAt" > ? ALLOW FILTERING`);
        expect(params.length).toBe(1);
    });
});
//# sourceMappingURL=SelectStatement.test.js.map