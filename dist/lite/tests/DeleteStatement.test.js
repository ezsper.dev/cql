"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Builder_1 = require("../Builder");
describe('Delete statements', () => {
    const builder = new Builder_1.Builder();
    it('Basic', () => {
        const { query, params } = builder
            .delete()
            .from('User')
            .whereEquals('id', '1423ef10-0801-4179-ba70-d8e5b3a2effc')
            .build();
        expect(query).toBe(`DELETE FROM "User" WHERE "id" = ?`);
        expect(params.length).toBe(1);
        expect(params[0]).toBe('1423ef10-0801-4179-ba70-d8e5b3a2effc');
    });
});
//# sourceMappingURL=DeleteStatement.test.js.map