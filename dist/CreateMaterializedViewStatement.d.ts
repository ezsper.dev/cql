import { SelectStatement } from './SelectStatement';
import { InsertStatement } from './InsertStatement';
import { UpdateStatement } from './UpdateStatement';
import { DeleteStatement } from './DeleteStatement';
import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import { TransformColumnDefinition, TransformPossibleColumnDefinition, Selection, StringKey } from './ColumnDefinition';
export interface MaterializedViewOptions {
    bloomFilterFpChance?: number;
    comment?: string;
    crcCheckChance?: number;
    dclocalReadRepairChange?: number;
    defaultTimeToLive?: number;
    gcGraceSeconds?: number;
    maxIndexInterval?: number;
    memtableFlushPeriodInMs?: number;
    minIndexInterval?: number;
    readRepairChance?: number;
    speculativeRetry?: string;
    caching?: {
        keys?: string;
        rowsPerPartition?: string;
    };
    compression?: {
        chunkLengthInKb?: number;
        class?: string;
        enabled?: boolean;
    };
    compaction?: {
        class?: string;
        maxThreshold?: number;
        minThreshold?: number;
    };
    [name: string]: any;
}
export declare class CreateMaterializedViewStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, I extends StringKey<R & S> = never, J extends StringKey<R & S> = never, N extends string = never> extends WhereStatement<S> {
    static Update: typeof UpdateStatement;
    static Insert: typeof InsertStatement;
    static Delete: typeof DeleteStatement;
    protected materializedViewDef: Raw;
    protected ifNotExistsDef: boolean;
    protected keyspaceDef: Raw;
    protected optionsDef: MaterializedViewOptions;
    protected columnFamilyDef: Raw;
    protected columnDefs: (keyof R)[];
    protected partitionKeyDefs: I[];
    protected clusteringKeyDefs: J[];
    protected clusteringOrderDefs: ([J, 'ASC' | 'DESC'])[];
    keyspace(name: string | Raw): this;
    as(name: string | Raw): this;
    as(keyspace: string | Raw, name: string | Raw): this;
    withOptions(options: MaterializedViewOptions): this;
    ifNotExists(value?: boolean): this;
    from(name: string | Raw): this;
    columns(columns: '*'): this;
    columns<B extends StringKey<R & S>>(...columns: B[]): CreateMaterializedViewStatement<Pick<R, B>, Pick<S, B>, never, never, N>;
    partitionKeys<B extends StringKey<R>>(...columns: B[]): CreateMaterializedViewStatement<R, S, B, never, N>;
    clusteringKeys<B extends StringKey<R>>(...columns: B[]): CreateMaterializedViewStatement<R, S, I, B, N>;
    withClusteringOrder(...clusteringOrders: ([J, 'ASC' | 'DESC'])[]): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
    select(): SelectStatement<R, S>;
    select(selection: '*'): SelectStatement<R, S, R>;
    select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, SS extends Selection<R, infer K, infer A> ? {
        [K2 in A]: R[K];
    } : never>;
    select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, Pick<R, K>>;
    select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, {
        [K in A]: R[K];
    }>;
}
