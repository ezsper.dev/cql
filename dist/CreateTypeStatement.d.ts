import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
import { ColumnDefinition, TransformColumnDefinition, TransformPossibleColumnDefinition } from './ColumnDefinition';
export declare class CreateTypeStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends BaseStatement {
    protected keyspaceDef: Raw;
    protected ifNotExistsDef: boolean;
    protected columnFamilyDef: Raw;
    protected columnDefs: ColumnDefinition;
    keyspace(name: string | Raw): this;
    as(name: string | Raw): this;
    as(keyspace: string | Raw, name: string | Raw): this;
    ifNotExists(value?: boolean): this;
    columns<B extends ColumnDefinition>(columns: B): CreateTypeStatement<TransformColumnDefinition<B>, TransformPossibleColumnDefinition<B>>;
    getColumnFamilyDef(): Readonly<Raw>;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
