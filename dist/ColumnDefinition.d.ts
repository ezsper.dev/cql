/// <reference types="node" />
import * as driver from 'cassandra-driver';
import { TypeDefinition } from './Type';
import { Raw } from './Raw';
import { TransformType, TransformPossibleType } from './TransformType';
export { TransformType, TransformPossibleType, } from './TransformType';
export interface Tuple<T> {
    elements: T;
    length: number;
    get(index: number): any;
    toString(): string;
    toJSON(): string;
    values(): T;
}
export declare type ColumnDefinition = {
    [name: string]: TypeDefinition;
};
export interface NativeTypeMap {
    ascii: string;
    bigint: typeof driver.types.Long;
    blob: Buffer;
    boolean: boolean;
    counter: typeof driver.types.Long;
    date: driver.types.LocalDate;
    decimal: driver.types.BigDecimal;
    double: number;
    float: number;
    inet: driver.types.InetAddress;
    int: number;
    smallint: number;
    text: string;
    time: driver.types.LocalTime;
    timestamp: Date;
    timeuuid: driver.types.TimeUuid;
    tinyint: number;
    uuid: driver.types.Uuid;
    varchar: string;
    varint: driver.types.Integer;
}
export interface PossibleNativeTypeMap {
    ascii: string;
    bigint: number | string | typeof driver.types.Long;
    blob: Buffer | string;
    boolean: boolean | string;
    counter: string | typeof driver.types.Long | number;
    date: string | driver.types.LocalDate;
    decimal: string | driver.types.BigDecimal;
    double: string | number;
    float: string | number;
    inet: string | driver.types.InetAddress;
    int: string | number;
    smallint: string | number;
    text: string;
    time: string | driver.types.LocalTime;
    timestamp: string | Date;
    timeuuid: string | driver.types.TimeUuid;
    tinyint: string | number;
    uuid: string | driver.types.Uuid;
    varchar: string | string;
    varint: string | driver.types.Integer;
}
export declare type FormatSet<T> = Set<T>;
export declare type FormatList<T> = T[];
export declare type FormatMap<K, V> = Map<K, V>;
export declare type FormatTuple<T> = Tuple<T>;
export declare type FormatPossibleSet<T> = Set<T> | T[];
export declare type FormatPossibleList<T> = T[];
export declare type FormatPossibleMap<K, V> = Map<K, V> | [K, V][];
export declare type FormatPossibleTuple<T> = Tuple<T> | T;
export declare type TransformColumnDefinition<C extends ColumnDefinition = ColumnDefinition> = {
    [K in StringKey<C>]: TransformType<C[K]>;
};
export declare type TransformPossibleColumnDefinition<C extends ColumnDefinition = ColumnDefinition> = {
    [K in StringKey<C>]: TransformPossibleType<C[K]>;
};
export declare type StringKey<T extends {}> = Exclude<keyof T, number | symbol>;
export interface SelectionAlias<S extends {}, K extends StringKey<S>, A extends string = A> {
    select: K;
    alias: A;
}
export declare type Selection<S extends {}, K extends StringKey<S> = StringKey<S>, A extends string = StringKey<S>> = SelectionAlias<S, K, A> | K;
export declare function getColumnType(def: TypeDefinition): Raw;
