"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SelectStatement_1 = require("./SelectStatement");
const UpdateStatement_1 = require("./UpdateStatement");
const InsertStatement_1 = require("./InsertStatement");
const DeleteStatement_1 = require("./DeleteStatement");
const CreateColumnFamilyStatement_1 = require("./CreateColumnFamilyStatement");
const CreateKeyspaceStatement_1 = require("./CreateKeyspaceStatement");
const CreateColumnFamilyIndexStatement_1 = require("./CreateColumnFamilyIndexStatement");
const CreateMaterializedViewStatement_1 = require("./CreateMaterializedViewStatement");
const Raw_1 = require("./Raw");
class Builder {
    constructor(options) {
        this.options = options;
    }
    token(...args) {
        const params = [];
        if (Array.isArray(args[0])) {
            return new Raw_1.Raw(`TOKEN(${args[0].map((value) => {
                if (value instanceof Raw_1.Raw) {
                    params.push(...value.params);
                    return value;
                }
                else {
                    params.push(value);
                    return '?';
                }
            }).join(', ')})`, params);
        }
        return new Raw_1.Raw(`TOKEN(${args.map(column => {
            if (column instanceof Raw_1.Raw) {
                params.push(...column.params);
                return column;
            }
            return `"${column}"`;
        }).join(', ')})`, params);
    }
    now() {
        return new Raw_1.Raw('NOW()');
    }
    ttl(column) {
        const params = [];
        if (column instanceof Raw_1.Raw) {
            params.push(...column.params);
        }
        return new Raw_1.Raw(`TTL(${column instanceof Raw_1.Raw ? column : `"${column}"`})`, params);
    }
    timestamp(column) {
        const params = [];
        if (column instanceof Raw_1.Raw) {
            params.push(...column.params);
        }
        return new Raw_1.Raw(`TIMESTAMP(${column instanceof Raw_1.Raw ? column : `"${column}"`})`, params);
    }
    key(column, key) {
        const params = [];
        if (column instanceof Raw_1.Raw) {
            params.push(...column.params);
        }
        params.push(key);
        return new Raw_1.Raw(`${column instanceof Raw_1.Raw ? column : `"${column}"`}[?]`, params);
    }
    createKeyspace(name) {
        const statement = new CreateKeyspaceStatement_1.CreateKeyspaceStatement(this);
        statement.as(name);
        return statement;
    }
    createIndex(...args) {
        const statement = new CreateColumnFamilyIndexStatement_1.CreateColumnFamilyIndexStatement(this);
        if (Array.isArray(args[0])) {
            statement.column(args[0][0], args[0][1]);
        }
        else {
            statement.column(args[0]);
        }
        if (args[1] != null) {
            statement.as(args[1]);
        }
        return statement;
    }
    createColumnFamily(...args) {
        const statement = new CreateColumnFamilyStatement_1.CreateColumnFamilyStatement(this);
        if (args.length === 2) {
            statement.as(args[0], args[1]);
        }
        else {
            statement.as(args[0]);
        }
        return statement;
    }
    createMaterializedView(...args) {
        const statement = new CreateMaterializedViewStatement_1.CreateMaterializedViewStatement(this);
        if (args.length === 2) {
            statement.as(args[0], args[1]);
        }
        else {
            statement.as(args[0]);
        }
        return statement;
    }
    select(...args) {
        const statement = new SelectStatement_1.SelectStatement(this);
        if (args.length === 2) {
            statement.from(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.from(args[0]);
        }
        return statement;
    }
    insert(...args) {
        const statement = new InsertStatement_1.InsertStatement(this);
        if (args.length === 2) {
            statement.into(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.into(args[0]);
        }
        return statement;
    }
    update(...args) {
        const statement = new UpdateStatement_1.UpdateStatement(this);
        if (args.length === 2) {
            statement.on(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.on(args[0]);
        }
        return statement;
    }
    delete(...args) {
        const statement = new DeleteStatement_1.DeleteStatement(this);
        if (args.length === 2) {
            statement.from(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.from(args[0]);
        }
        return statement;
    }
    filterResult(result) {
        return result;
    }
    mapRow(index, row) {
        return row;
    }
}
Builder.Update = UpdateStatement_1.UpdateStatement;
Builder.Insert = InsertStatement_1.InsertStatement;
Builder.Delete = DeleteStatement_1.DeleteStatement;
Builder.CreateKeyspace = CreateKeyspaceStatement_1.CreateKeyspaceStatement;
Builder.CreateColumnFamily = CreateColumnFamilyStatement_1.CreateColumnFamilyStatement;
Builder.CreateColumnFamilyIndex = CreateColumnFamilyIndexStatement_1.CreateColumnFamilyIndexStatement;
Builder.CreateMaterializedView = CreateMaterializedViewStatement_1.CreateMaterializedViewStatement;
exports.Builder = Builder;
//# sourceMappingURL=Builder.js.map