"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
describe('CreateColumnFamilyStatement', () => {
    const builder = new __1.Builder();
    it('With Basic Options', () => {
        const columnFamily = builder
            .createColumnFamily('global', 'User')
            .columns({
            id: __1.Type.text,
            accountId: __1.Type.text,
            displayName: __1.Type.text,
            createdAt: __1.Type.timestamp,
            updatedAt: __1.Type.timestamp,
        })
            .partitionKeys('id')
            .withOptions({
            bloomFilterFpChance: 0.01,
            comment: '',
            crcCheckChance: 1.0,
            dclocalReadRepairChance: 0.1,
            defaultTimeToLive: 0,
            gcGraceSeconds: 864000,
            maxIndexInterval: 2048,
            memtableFlushPeriodInMs: 0,
            minIndexInterval: 128,
            readRepairChance: 0.0,
            speculativeRetry: '99.0PERCENTILE',
            caching: {
                keys: 'ALL',
                rowsPerPartition: 'NONE',
            },
            compression: {
                chunkLengthInKb: 64,
                class: 'LZ4Compressor',
                enabled: true
            },
            compaction: {
                class: 'SizeTieredCompactionStrategy',
                maxThreshold: 32,
                minThreshold: 4
            },
        });
        const { query, params } = columnFamily.build();
        expect(query).toBe(`CREATE TABLE "global"."User" ("id" text, "accountId" text, "displayName" text, "createdAt" timestamp, "updatedAt" timestamp, PRIMARY KEY ("id")) WITH BLOOM_FILTER_FP_CHANCE = 0.01 AND COMMENT = '' AND CRC_CHECK_CHANCE = 1 AND DCLOCAL_READ_REPAIR_CHANCE = 0.1 AND DEFAULT_TIME_TO_LIVE = 0 AND GC_GRACE_SECONDS = 864000 AND MAX_INDEX_INTERVAL = 2048 AND MEMTABLE_FLUSH_PERIOD_IN_MS = 0 AND MIN_INDEX_INTERVAL = 128 AND READ_REPAIR_CHANCE = 0 AND SPECULATIVE_RETRY = '99.0PERCENTILE' AND CACHING = { 'keys': 'ALL', 'rows_per_partition': 'NONE' } AND COMPRESSION = { 'chunk_length_in_kb': 64, 'class': 'LZ4Compressor', 'enabled': true } AND COMPACTION = { 'class': 'SizeTieredCompactionStrategy', 'max_threshold': 32, 'min_threshold': 4 }`);
        expect(params.length).toBe(0);
    });
    it('With Clustering', () => {
        const columnFamily = builder
            .createColumnFamily('global', 'UserByName')
            .columns({
            id: __1.Type.text,
            accountId: __1.Type.text,
            displayName: __1.Type.text,
        })
            .partitionKeys('accountId')
            .clusteringKeys('displayName', 'id')
            .withOptions({
            comment: '',
        })
            .withClusteringOrder(['displayName', 'ASC'], ['id', 'ASC']);
        const { query, params } = columnFamily.build();
        expect(query).toBe(`CREATE TABLE "global"."UserByName" ("id" text, "accountId" text, "displayName" text, PRIMARY KEY ("accountId", "displayName", "id")) WITH COMMENT = '' AND CLUSTERING ORDER BY ("displayName" ASC, "id" ASC)`);
        expect(params.length).toBe(0);
    });
});
//# sourceMappingURL=CreateColumnFamilyStatement.test.js.map