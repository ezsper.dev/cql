"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SelectStatement_1 = require("./SelectStatement");
const InsertStatement_1 = require("./InsertStatement");
const UpdateStatement_1 = require("./UpdateStatement");
const DeleteStatement_1 = require("./DeleteStatement");
const WhereStatement_1 = require("./WhereStatement");
const CreateKeyspaceStatement_1 = require("./CreateKeyspaceStatement");
const Raw_1 = require("./Raw");
class CreateMaterializedViewStatement extends WhereStatement_1.WhereStatement {
    constructor() {
        super(...arguments);
        this.columnDefs = [];
        this.partitionKeyDefs = [];
        this.clusteringKeyDefs = [];
        this.clusteringOrderDefs = [];
    }
    keyspace(name) {
        if (name instanceof Raw_1.Raw) {
            this.keyspaceDef = name;
        }
        else {
            this.keyspaceDef = new Raw_1.Raw(`"${name}"`);
        }
        return this;
    }
    as(...args) {
        if (args.length === 1) {
            if (args[0] instanceof Raw_1.Raw) {
                this.materializedViewDef = args[0];
            }
            else {
                this.materializedViewDef = new Raw_1.Raw(`"${args[0]}"`);
            }
        }
        else {
            this.keyspace(args[0]);
            if (args[1] instanceof Raw_1.Raw) {
                this.materializedViewDef = args[1];
            }
            else {
                this.materializedViewDef = new Raw_1.Raw(`"${args[1]}"`);
            }
        }
        return this;
    }
    withOptions(options) {
        this.optionsDef = options;
        return this;
    }
    ifNotExists(value = true) {
        this.ifNotExistsDef = value;
        return this;
    }
    from(name) {
        this.columnFamilyDef = name instanceof Raw_1.Raw ? name : new Raw_1.Raw(`"${name}"`);
        return this;
    }
    columns(...args) {
        if (args[0] === '*') {
            this.columnDefs = [];
        }
        else {
            this.columnDefs = args;
        }
        return this;
    }
    partitionKeys(...columns) {
        this.partitionKeyDefs = columns;
        for (const column of columns) {
            this.whereIsNotNull(column);
        }
        return this;
    }
    clusteringKeys(...columns) {
        this.clusteringKeyDefs = columns;
        for (const column of columns) {
            this.whereIsNotNull(column);
        }
        return this;
    }
    withClusteringOrder(...clusteringOrders) {
        this.clusteringOrderDefs = clusteringOrders;
        return this;
    }
    validate() {
        if (this.columnFamilyDef == null) {
            throw new Error('The TABLE name not defined');
        }
    }
    getParams() {
        const params = [];
        params.push(...this.materializedViewDef.params);
        params.push(...this.columnFamilyDef.params);
        params.push(...this.getWhereParams());
        return params;
    }
    getQuery() {
        let query = `CREATE MATERIALIZED VIEW ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.materializedViewDef} AS SELECT `;
        if (this.columnDefs.length) {
            query += this.columnDefs.map(column => `"${column}"`).join(', ');
        }
        else {
            query += '*';
        }
        query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.` : ''}${this.columnFamilyDef}`;
        query += this.getWhereQuery();
        const primaryKey = [];
        if (this.partitionKeyDefs.length === 1) {
            primaryKey.push(...this.partitionKeyDefs);
        }
        else {
            primaryKey.push(this.partitionKeyDefs);
        }
        primaryKey.push(...this.clusteringKeyDefs);
        query += ` PRIMARY KEY (${primaryKey
            .map(column => Array.isArray(column)
            ? `(${column.map(partition => `"${partition}"`).join(', ')})`
            : `"${column}"`).join(', ')})`;
        const withDefs = [];
        if (this.optionsDef != null) {
            withDefs.push(...CreateKeyspaceStatement_1.buildOptions(this.optionsDef));
        }
        if (this.clusteringOrderDefs.length) {
            withDefs.push(new Raw_1.Raw(`CLUSTERING ORDER BY (${this.clusteringOrderDefs.map(([column, order]) => `"${column}" ${order}`).join(', ')})`));
        }
        if (withDefs.length) {
            query += ` WITH ${withDefs.join(' AND ')}`;
        }
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
    select(...args) {
        const statement = new SelectStatement_1.SelectStatement(this.builder);
        if (args.length === 2) {
            statement.select(args[0], args[1]);
        }
        else if (args.length === 1) {
            statement.select(args[0]);
        }
        if (typeof this.keyspaceDef !== 'undefined') {
            statement.keyspace(this.keyspaceDef);
        }
        statement.from(this.materializedViewDef);
        return statement;
    }
}
CreateMaterializedViewStatement.Update = UpdateStatement_1.UpdateStatement;
CreateMaterializedViewStatement.Insert = InsertStatement_1.InsertStatement;
CreateMaterializedViewStatement.Delete = DeleteStatement_1.DeleteStatement;
exports.CreateMaterializedViewStatement = CreateMaterializedViewStatement;
//# sourceMappingURL=CreateMaterializedViewStatement.js.map