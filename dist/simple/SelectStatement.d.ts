import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import { StringKey, Selection, TransformColumnDefinition, TransformPossibleColumnDefinition } from './ColumnDefinition';
export declare class SelectStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, P extends {} = {}> extends WhereStatement<S, P> {
    protected selectionDefs: Raw[];
    protected keyspaceDef: Raw;
    protected columnFamilyDef: Raw;
    protected orderByDefs: Raw[];
    protected limitDef?: number;
    protected distinctDef: boolean;
    protected allowFilteringDef: boolean;
    distinct(value?: boolean): this;
    count(): this;
    select(selection: '*'): SelectStatement<R, S, R>;
    select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, P & (SS extends Selection<R, infer K, infer A> ? {
        [K2 in A]: R[K];
    } : never)>;
    select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, P & Pick<R, K>>;
    select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, P & {
        [K in A]: R[K];
    }>;
    keyspace(name: string | Raw): this;
    from(name: string | Raw): this;
    from(keyspace: string | Raw, name: string | Raw): this;
    orderBy<K extends StringKey<R>>(name: K, order: 'ASC' | 'DESC'): this;
    limit(value: number): this;
    allowFiltering(value?: boolean): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
