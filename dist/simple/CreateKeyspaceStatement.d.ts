import { BaseStatement } from './BaseStatement';
import { CreateColumnFamilyStatement } from './CreateColumnFamilyStatement';
import { Raw } from './Raw';
export interface KeyspaceColumnFamilies {
    [name: string]: CreateColumnFamilyStatement;
}
export interface KeyspaceReplicationSimpleStrategyOptions {
    class: 'SimpleStrategy';
    replicationFactor: number;
}
export interface KeyspaceReplicationNetworkTopologyStrategyOptions {
    class: 'NetworkTopologyStrategy';
    replicationFactor: {
        [datacenter: string]: number;
    };
}
export declare type KeyspaceReplicationOptions = KeyspaceReplicationSimpleStrategyOptions | KeyspaceReplicationNetworkTopologyStrategyOptions;
export interface KeyspaceOptions {
    durableWrites?: boolean;
    replication?: KeyspaceReplicationOptions;
    [name: string]: any;
}
export declare function buildOptionValue(value: any): string;
export declare function buildOptions(options: {
    [name: string]: any;
}): Raw[];
export declare class CreateKeyspaceStatement<C extends KeyspaceColumnFamilies = {}> extends BaseStatement {
    static CreateColumnFamily: typeof CreateColumnFamilyStatement;
    protected keyspaceDef: Raw;
    protected columnDefs: C;
    protected optionsDef: KeyspaceOptions;
    protected ifNotExistsDef: boolean;
    as(name: string | Raw): this;
    withOptions(options: KeyspaceOptions): this;
    createColumnFamily(name: string | Raw): CreateColumnFamilyStatement;
    ifNotExists(value?: boolean): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
