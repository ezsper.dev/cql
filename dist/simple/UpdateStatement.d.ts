import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import { StringKey, TransformPossibleColumnDefinition } from './ColumnDefinition';
export declare class UpdateStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends WhereStatement<S> {
    protected keyspaceDef: Raw;
    protected columnFamilyDef: Raw;
    protected setClauseDefs: Raw[];
    protected ttl: number;
    protected timestamp: number;
    keyspace(name: string | Raw): this;
    on(name: string | Raw): this;
    on(keyspace: string | Raw, name: string | Raw): this;
    protected setValue<K extends StringKey<S>>(selection: K, operator: '+' | '=' | '-', value: S[K]): this;
    set(data: Partial<{
        [K in StringKey<S>]: S[K] | null;
    }>): this;
    set<K extends StringKey<S>>(selection: K, value: S[K] | null): this;
    add<K extends StringKey<S>>(selection: K, value: S[K]): this;
    remove<K extends StringKey<S>>(selection: K, value: S[K]): this;
    increment<K extends StringKey<S>>(selection: K, value: S[K]): this;
    decrement<K extends StringKey<S>>(selection: K, value: S[K]): this;
    usingTTL(unix: number): this;
    usingTTL(date: Date): this;
    usingTTL(interval: string): this;
    usingTimestamp(unix: number): this;
    usingTimestamp(date: Date): this;
    usingTimestamp(interval: string): this;
    protected validate(): this;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
