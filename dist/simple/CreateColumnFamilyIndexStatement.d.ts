import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
export declare class CreateColumnFamilyIndexStatement extends BaseStatement {
    protected secondaryIndexDef: Raw;
    protected columnFamilyDef: Raw;
    protected columnDef: Raw;
    protected keyspaceDef: Raw;
    protected ifNotExistsDef: boolean;
    ifNotExists(value?: boolean): this;
    keyspace(name: string | Raw): this;
    as(name: string | Raw): this;
    on(name: string | Raw): this;
    on(keyspace: string | Raw, name: string | Raw): this;
    column(name: string | Raw, type?: 'FULL' | 'ENTRIES' | 'VALUES' | 'KEYS'): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
