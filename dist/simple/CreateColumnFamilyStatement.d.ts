import { BaseStatement } from './BaseStatement';
import { SelectStatement } from './SelectStatement';
import { InsertStatement } from './InsertStatement';
import { UpdateStatement } from './UpdateStatement';
import { DeleteStatement } from './DeleteStatement';
import { CreateMaterializedViewStatement } from './CreateMaterializedViewStatement';
import { CreateColumnFamilyIndexStatement } from './CreateColumnFamilyIndexStatement';
import { Raw } from './Raw';
import { ColumnDefinition, TransformColumnDefinition, TransformPossibleColumnDefinition, Selection, StringKey } from './ColumnDefinition';
export interface ColumnFamilyOptions {
    bloomFilterFpChance?: number;
    comment?: string;
    crcCheckChance?: number;
    dclocalReadRepairChange?: number;
    defaultTimeToLive?: number;
    gcGraceSeconds?: number;
    maxIndexInterval?: number;
    memtableFlushPeriodInMs?: number;
    minIndexInterval?: number;
    readRepairChance?: number;
    speculativeRetry?: string;
    caching?: {
        keys?: string;
        rowsPerPartition?: string;
    };
    compression?: {
        chunkLengthInKb?: number;
        class?: string;
        enabled?: boolean;
    };
    compaction?: {
        class?: string;
        maxThreshold?: number;
        minThreshold?: number;
    };
    [name: string]: any;
}
export declare class CreateColumnFamilyStatement<R extends TransformColumnDefinition = TransformColumnDefinition, S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition, I extends StringKey<R> = never, J extends Exclude<StringKey<R>, I> = never, N extends string = never> extends BaseStatement {
    static Update: typeof UpdateStatement;
    static Insert: typeof InsertStatement;
    static Delete: typeof DeleteStatement;
    static CreateColumnFamilyIndex: typeof CreateColumnFamilyIndexStatement;
    static CreateMaterializedView: typeof CreateMaterializedViewStatement;
    protected nullableColumns: N[];
    protected keyspaceDef: Raw;
    protected ifNotExistsDef: boolean;
    protected columnFamilyDef: Raw;
    protected columnDefs: ColumnDefinition;
    protected partitionKeyDefs: I[];
    protected clusteringKeyDefs: J[];
    protected clusteringOrderDefs: ([J, 'ASC' | 'DESC'])[];
    protected optionsDef: ColumnFamilyOptions;
    keyspace(name: string | Raw): this;
    as(name: string | Raw): this;
    as(keyspace: string | Raw, name: string | Raw): this;
    ifNotExists(value?: boolean): this;
    columns<B extends ColumnDefinition>(columns: B): CreateColumnFamilyStatement<TransformColumnDefinition<B>, TransformPossibleColumnDefinition<B>>;
    partitionKeys<B extends StringKey<R>>(...columns: B[]): CreateColumnFamilyStatement<R, S, B, never, N>;
    clusteringKeys<B extends Exclude<StringKey<R>, I>>(...columns: B[]): CreateColumnFamilyStatement<R, S, I, B, N>;
    nullable<B extends StringKey<R>>(...columns: Exclude<B, I | J>[]): CreateColumnFamilyStatement<R, S, I, J, B>;
    withOptions(options: ColumnFamilyOptions): this;
    withClusteringOrder(...clusteringOrders: ([J, 'ASC' | 'DESC'])[]): this;
    createMaterializedView(name: string | Raw): CreateMaterializedViewStatement<R, S, I, J, N>;
    createMaterializedView(keyspace: string, name: string): CreateMaterializedViewStatement<R, S, I, J, N>;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
    isModel(data: any): data is CFModel<this>;
    isInsertInput(data: any): data is CFInsertInput<this>;
    isUpdateInput(data: any): data is CFUpdateInput<this>;
    createIndex<K extends StringKey<R>>(column: K | Raw, name?: string | Raw): CreateColumnFamilyIndexStatement;
    createIndex<K extends StringKey<R>>(column: [K | Raw, 'FULL' | 'ENTRIES' | 'KEYS' | 'VALUES'], name?: string | Raw): CreateColumnFamilyIndexStatement;
    select(): SelectStatement<R, S>;
    select(selection: '*'): SelectStatement<R, S, R>;
    select<SS extends Selection<R>>(selection: SS): SelectStatement<R, S, SS extends Selection<R, infer K, infer A> ? {
        [K2 in A]: R[K];
    } : never>;
    select<K extends StringKey<R>>(select: K[]): SelectStatement<R, S, Pick<R, K>>;
    select<K extends StringKey<R>, A extends string>(select: K, alias: A): SelectStatement<R, S, {
        [K in A]: R[K];
    }>;
    insert(): InsertStatement<S>;
    insert(data: Partial<{
        [K in StringKey<S>]: S[K];
    }>): InsertStatement<S>;
    insert<K extends StringKey<R>>(select: K, replace: S[K]): InsertStatement<S>;
    update(): UpdateStatement<S>;
    update(data: Partial<{
        [K in StringKey<S>]: S[K];
    }>): UpdateStatement<S>;
    update<K extends StringKey<S>>(data: ([K, S[K]])[]): UpdateStatement<S>;
    update<K extends StringKey<S>>(selection: K, value: S[K]): UpdateStatement<S>;
    delete(): DeleteStatement<S>;
    delete<K extends StringKey<S>>(columns: K[]): DeleteStatement<S>;
}
export declare type CFModel<T> = T extends CreateColumnFamilyStatement<infer R, any, infer I, infer J, infer N> ? Pick<R, I | J | Exclude<StringKey<R>, N>> & Partial<Pick<R, Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>>> : (T extends CreateMaterializedViewStatement<infer R, any, infer I, infer J, infer N> ? Pick<R, I | J | Exclude<StringKey<R>, N>> & Partial<Pick<R, Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>>> : any);
export declare type CFUpdateInput<T> = T extends CreateColumnFamilyStatement<infer R, infer S, infer I, infer J, infer N> ? ({
    [K in Exclude<StringKey<R>, N | I | J>]?: S[K];
} & {
    [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null;
}) : (T extends CreateMaterializedViewStatement<infer R, infer S, infer I, infer J, infer N> ? ({
    [K in Exclude<StringKey<R>, N | I | J>]?: S[K];
} & {
    [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null;
}) : any);
export declare type CFInsertInput<T> = T extends CreateColumnFamilyStatement<infer R, infer S, infer I, infer J, infer N> ? ({
    [K in I | J | Exclude<StringKey<R>, N>]: S[K];
} & {
    [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null;
}) : (T extends CreateMaterializedViewStatement<infer R, infer S, infer I, infer J, infer N> ? ({
    [K in I | J | Exclude<StringKey<R>, N>]: S[K];
} & {
    [K in Exclude<StringKey<R>, I | J | Exclude<StringKey<R>, N>>]?: S[K] | null;
}) : any);
