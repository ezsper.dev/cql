export * from './Raw';
export * from './Type';
export { Type as type } from './Type';
export * from './ColumnDefinition';
export * from './BaseStatement';
export * from './SelectStatement';
export * from './UpdateStatement';
export * from './InsertStatement';
export * from './DeleteStatement';
export * from './CreateKeyspaceStatement';
export * from './CreateColumnFamilyStatement';
export * from './CreateColumnFamilyIndexStatement';
export * from './CreateMaterializedViewStatement';
export * from './CreateTypeStatement';
export * from './Builder';
declare let token: {
    (values: any[]): import("./Raw").Raw;
    (...columns: (string | import("./Raw").Raw)[]): import("./Raw").Raw;
}, ttl: (column: string | import("./Raw").Raw) => import("./Raw").Raw, now: () => import("./Raw").Raw, timestamp: (column: string | import("./Raw").Raw) => import("./Raw").Raw, deleteRow: {
    (name: string | import("./Raw").Raw): import("./DeleteStatement").DeleteStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (keyspace: string, name: string): import("./DeleteStatement").DeleteStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (): import("./DeleteStatement").DeleteStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
}, select: {
    (name: string | import("./Raw").Raw): import("./SelectStatement").SelectStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, {}>;
    (keyspace: string, name: string): import("./SelectStatement").SelectStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, {}>;
    (): import("./SelectStatement").SelectStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, {}>;
}, update: {
    (name: string | import("./Raw").Raw): import("./UpdateStatement").UpdateStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (keyspace: string, name: string): import("./UpdateStatement").UpdateStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (): import("./UpdateStatement").UpdateStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
}, insert: {
    (name: string | import("./Raw").Raw): import("./InsertStatement").InsertStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (keyspace: string, name: string): import("./InsertStatement").InsertStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
    (): import("./InsertStatement").InsertStatement<import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>>;
}, createKeyspace: (name: string | import("./Raw").Raw) => import("./CreateKeyspaceStatement").CreateKeyspaceStatement<{}>, createColumnFamily: {
    (name: string | import("./Raw").Raw): import("./CreateColumnFamilyStatement").CreateColumnFamilyStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, never, never, never>;
    (keyspace: string, name: string): import("./CreateColumnFamilyStatement").CreateColumnFamilyStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, never, never, never>;
}, createMaterializedView: {
    (name: string | import("./Raw").Raw): import("./CreateMaterializedViewStatement").CreateMaterializedViewStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, never, never, never>;
    (keyspace: string, name: string): import("./CreateMaterializedViewStatement").CreateMaterializedViewStatement<import("./ColumnDefinition").TransformColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, import("./ColumnDefinition").TransformPossibleColumnDefinition<import("./ColumnDefinition").ColumnDefinition>, never, never, never>;
}, createIndex: {
    (column: string | import("./Raw").Raw, name?: string | import("./Raw").Raw | undefined): import("./CreateColumnFamilyIndexStatement").CreateColumnFamilyIndexStatement;
    (column: [string | import("./Raw").Raw, "FULL" | "ENTRIES" | "VALUES" | "KEYS"], name?: string | import("./Raw").Raw | undefined): import("./CreateColumnFamilyIndexStatement").CreateColumnFamilyIndexStatement;
};
export { token, ttl, now, timestamp, deleteRow, select, update, insert, createKeyspace, createColumnFamily, createMaterializedView, createIndex, };
