"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = (() => {
    const builder = {};
    const singleTypes = [
        'ascii',
        'bigint',
        'blob',
        'boolean',
        'counter',
        'date',
        'decimal',
        'double',
        'float',
        'inet',
        'int',
        'smallint',
        'text',
        'time',
        'timestamp',
        'timeuuid',
        'tinyint',
        'uuid',
        'varchar',
        'varint',
    ];
    for (const typeName of singleTypes) {
        builder[typeName] = { type: typeName };
    }
    builder.type = (type) => ({ type, frozen: false });
    builder.set = (type) => ({ set: type, frozen: false });
    builder.list = (type) => ({ list: type, frozen: false });
    builder.map = (key, value) => ({ map: [key, value], frozen: false });
    builder.tuple = (types) => ({ tuple: types, frozen: false });
    builder.frozenType = (type) => ({ type, frozen: true });
    builder.frozenSet = (type) => ({ set: type, frozen: true });
    builder.frozenList = (type) => ({ list: type, frozen: true });
    builder.frozenMap = (key, value) => ({ map: [key, value], frozen: true });
    builder.frozenTuple = (types) => ({ tuple: types, frozen: true });
    return builder;
})();
//# sourceMappingURL=Type.js.map