"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const Raw_1 = require("./Raw");
class WhereStatement extends BaseStatement_1.BaseStatement {
    constructor() {
        super(...arguments);
        this.whereClauseDefs = [];
    }
    where(...args) {
        if (args.length > 1) {
            const params = [];
            const [selection, comparator, value] = args;
            const joinValues = (values) => `(${values.map((def) => {
                if (Array.isArray(selection) && comparator === 'IN') {
                    def.forEach((defItem) => {
                        if (defItem instanceof Raw_1.Raw) {
                            params.push(...defItem.params);
                            return def.query;
                        }
                        params.push(defItem);
                    });
                    return `(${selection.map(() => '?').join(', ')})`;
                }
                if (def instanceof Raw_1.Raw) {
                    params.push(...def.params);
                    return def.query;
                }
                params.push(def);
                return '?';
            }).join(', ')})`;
            if (Array.isArray(selection)) {
                const query = `(${selection.map((def) => {
                    if (def instanceof Raw_1.Raw) {
                        params.push(...def.params);
                        return def.query;
                    }
                    return `"${def}"`;
                }).join(', ')}) ${comparator} ${joinValues(value)}`;
                this.whereClauseDefs.push(new Raw_1.Raw(query, params));
            }
            else {
                const params = [];
                if (selection instanceof Raw_1.Raw) {
                    params.push(...selection.params);
                }
                if (value instanceof Raw_1.Raw) {
                    params.push(...value.params);
                }
                else if (typeof value !== 'undefined') {
                    if (comparator === 'IN') {
                        params.push(...value);
                    }
                    else {
                        params.push(value);
                    }
                }
                const query = `${selection instanceof Raw_1.Raw
                    ? selection.query
                    : `"${selection}"`} ${comparator}${comparator === 'IN'
                    ? ` ${joinValues(value)}`
                    : (comparator === 'IS NOT NULL'
                        ? ''
                        : ` ${(value instanceof Raw_1.Raw ? value.query : '?')}`)}`;
                this.whereClauseDefs.push(new Raw_1.Raw(query, params));
            }
            return this;
        }
        if (args[0] instanceof Raw_1.Raw) {
            this.whereClauseDefs.push(args[0]);
        }
        else if (Array.isArray(args[0])) {
            this.where(...args[0]);
        }
        else {
            for (const key in args[0]) {
                this.where(key, '=', args[0][key]);
            }
        }
        return this;
    }
    whereIsNotNull(key) {
        this.where(key, 'IS NOT NULL');
        return this;
    }
    whereIn(...args) {
        if (args.length === 2) {
            this.where(args[0], 'IN', args[1]);
        }
        else {
            const keys = Object.keys(args[0]);
            const values = Object.values(args[0]);
            return this.where(keys, 'IN', values);
        }
        return this;
    }
    whereContains(key, value) {
        this.where(key, 'CONTAINS', value);
        return this;
    }
    whereLowerThan(key, value) {
        this.where(key, '<', value);
        return this;
    }
    whereGreaterThan(key, value) {
        this.where(key, '>', value);
        return this;
    }
    whereEquals(key, value) {
        this.where(key, '=', value);
        return this;
    }
    hasWhereClauses() {
        return this.whereClauseDefs.length > 0;
    }
    getWhereParams() {
        const params = [];
        for (const def of this.whereClauseDefs) {
            params.push(...def.params);
        }
        return params;
    }
    getWhereQuery() {
        if (!this.hasWhereClauses()) {
            return '';
        }
        let query = ' WHERE ';
        query += this.whereClauseDefs.map(where => where.query).join(' AND ');
        return query;
    }
}
exports.WhereStatement = WhereStatement;
//# sourceMappingURL=WhereStatement.js.map