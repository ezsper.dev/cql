export declare class Raw {
    readonly query: string;
    readonly params: any[];
    constructor(query: string, params?: any[]);
    toString(): string;
}
export declare function raw(strings: TemplateStringsArray, ...values: any[]): Raw;
