import { CreateTypeStatement } from './CreateTypeStatement';
import { ColumnDefinition } from './ColumnDefinition';
export declare type RawTypeDefinition = SingleTypeDefinition | NativeTypeDefinition | RawSetDefinition | RawListDefinition | RawMapDefinition | RawTupleDefinition;
export declare type NativeTypeKeys = Exclude<keyof NativeType, number | symbol>;
export interface NativeTypeDefinition<T extends NativeTypeKeys = NativeTypeKeys> {
    type: T;
}
export interface SingleTypeDefinition {
    type: CreateTypeStatement<ColumnDefinition>;
    frozen: boolean;
}
export interface RawSetDefinition {
    set: RawTypeDefinition;
    frozen: boolean;
}
export interface SetDefinition<T extends RawTypeDefinition = RawTypeDefinition> {
    set: T;
    frozen: boolean;
}
export interface RawListDefinition {
    list: RawTypeDefinition;
    frozen: boolean;
}
export interface ListDefinition<T extends RawTypeDefinition = RawTypeDefinition> {
    list: T;
    frozen: boolean;
}
export interface RawMapDefinition {
    map: [RawTypeDefinition, RawTypeDefinition];
    frozen: boolean;
}
export interface MapDefinition<K extends RawTypeDefinition = RawTypeDefinition, V extends RawTypeDefinition = RawTypeDefinition> {
    map: [K, V];
    frozen: boolean;
}
export declare type RawTupleDefinitionTuple = [RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | [RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition, RawTypeDefinition] | any[];
export interface RawTupleDefinition {
    tuple: RawTupleDefinitionTuple;
    frozen: boolean;
}
export interface TupleDefinition<T extends RawTupleDefinitionTuple = RawTupleDefinitionTuple> {
    tuple: T;
    frozen: boolean;
}
export declare type TypeDefinition = SingleTypeDefinition | NativeTypeDefinition | SetDefinition | ListDefinition | MapDefinition | TupleDefinition;
export interface Type extends SingleType, NativeType, SetType, ListType, MapType, TupleType {
}
export interface NativeType {
    ascii: {
        type: 'ascii';
    };
    bigint: {
        type: 'bigint';
    };
    blob: {
        type: 'blob';
    };
    boolean: {
        type: 'boolean';
    };
    counter: {
        type: 'counter';
    };
    date: {
        type: 'date';
    };
    decimal: {
        type: 'decimal';
    };
    double: {
        type: 'double';
    };
    float: {
        type: 'float';
    };
    inet: {
        type: 'inet';
    };
    int: {
        type: 'int';
    };
    smallint: {
        type: 'smallint';
    };
    text: {
        type: 'text';
    };
    time: {
        type: 'time';
    };
    timestamp: {
        type: 'timestamp';
    };
    timeuuid: {
        type: 'timeuuid';
    };
    tinyint: {
        type: 'tinyint';
    };
    uuid: {
        type: 'uuid';
    };
    varchar: {
        type: 'varchar';
    };
    varint: {
        type: 'varint';
    };
}
export interface SingleType {
    type<S extends CreateTypeStatement>(createType: S): {
        type: S;
        frozen: false;
    };
    type<T extends NativeTypeKeys>(type: T): {
        type: T;
    };
    frozenType<S extends CreateTypeStatement>(createType: S): {
        type: S;
        frozen: true;
    };
}
export interface TupleType {
    tuple<T1 extends TypeDefinition>(a: T1): {
        tuple: [T1];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition>(a: T1, b: T2): {
        tuple: [T1, T2];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition>(a: T1, b: T2, c: T3): {
        tuple: [T1, T2, T3];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4): {
        tuple: [T1, T2, T3, T4];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5): {
        tuple: [T1, T2, T3, T4, T5];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6): {
        tuple: [T1, T2, T3, T4, T5, T6];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7): {
        tuple: [T1, T2, T3, T4, T5, T6, T7];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition, T9 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8, i: T9): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8, T9];
        frozen: false;
    };
    tuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition, T9 extends TypeDefinition, T10 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8, i: T9, j: T10): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8, T9, T10];
        frozen: false;
    };
    tuple(...types: any[]): {
        tuple: any[];
        frozen: false;
    };
    frozenTuple<T1 extends TypeDefinition>(a: T1): {
        tuple: [T1];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition>(a: T1, b: T2): {
        tuple: [T1, T2];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition>(a: T1, b: T2, c: T3): {
        tuple: [T1, T2, T3];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4): {
        tuple: [T1, T2, T3, T4];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5): {
        tuple: [T1, T2, T3, T4, T5];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6): {
        tuple: [T1, T2, T3, T4, T5, T6];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7): {
        tuple: [T1, T2, T3, T4, T5, T6, T7];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition, T9 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8, i: T9): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8, T9];
        frozen: true;
    };
    frozenTuple<T1 extends TypeDefinition, T2 extends TypeDefinition, T3 extends TypeDefinition, T4 extends TypeDefinition, T5 extends TypeDefinition, T6 extends TypeDefinition, T7 extends TypeDefinition, T8 extends TypeDefinition, T9 extends TypeDefinition, T10 extends TypeDefinition>(a: T1, b: T2, c: T3, d: T4, e: T5, f: T6, g: T7, h: T8, i: T9, j: T10): {
        tuple: [T1, T2, T3, T4, T5, T6, T7, T8, T9, T10];
        frozen: true;
    };
    frozenTuple(...types: any[]): {
        tuple: any[];
        frozen: true;
    };
}
export interface SetType {
    set<T extends TypeDefinition>(type: T): {
        set: T;
        frozen: false;
    };
    frozenSet<T extends TypeDefinition>(type: T): {
        set: T;
        frozen: true;
    };
}
export interface ListType {
    list<T extends TypeDefinition>(type: T): {
        list: T;
        frozen: false;
    };
    frozenList<T extends TypeDefinition>(type: T): {
        list: T;
        frozen: true;
    };
}
export interface MapType {
    map<K extends TypeDefinition, V extends TypeDefinition>(key: K, value: V): {
        map: [K, V];
        frozen: false;
    };
    frozenMap<K extends TypeDefinition, V extends TypeDefinition>(key: K, value: V): {
        map: [K, V];
        frozen: true;
    };
}
export declare const Type: Type;
