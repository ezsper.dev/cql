"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class BaseStatement {
    constructor(builder) {
        this.builder = builder;
    }
    build() {
        throw new Error('Cannot execute BaseStatement');
    }
    execute(client, options) {
        return __awaiter(this, void 0, void 0, function* () {
            const { query, params } = this.build();
            const result = this.builder.filterResult(client.execute(query, params, Object.assign({}, options, { prepare: true })));
            result.rows = result.rows.map((row, index) => this.builder.mapRow(index, row));
            return result;
        });
    }
    eachRown(client, options, iterator, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            const { query, params } = this.build();
            client.eachRow(query, params, Object.assign({}, options, { prepare: true }), (index, row) => {
                if (typeof iterator === 'function') {
                    iterator(index, this.builder.mapRow(index, row));
                }
            }, callback);
        });
    }
}
exports.BaseStatement = BaseStatement;
//# sourceMappingURL=BaseStatement.js.map