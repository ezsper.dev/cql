"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseStatement_1 = require("./BaseStatement");
const CreateColumnFamilyStatement_1 = require("./CreateColumnFamilyStatement");
const Raw_1 = require("./Raw");
const snakeCase = require('lodash.snakecase');
function buildOptionValue(value) {
    if (typeof value === 'object' && value != null) {
        const options = [];
        for (const key in value) {
            options.push(`'${snakeCase(key)}': ${buildOptionValue(value[key])}`);
        }
        return `{ ${options.join(', ')} }`;
    }
    if (Array.isArray(value)) {
        return `[${value.map(buildOptionValue).join(', ')}]`;
    }
    return JSON.stringify(value).replace(/(^\"|\"$)/g, '\'');
}
exports.buildOptionValue = buildOptionValue;
function buildOptions(options) {
    const keys = [];
    for (const key in options) {
        keys.push(new Raw_1.Raw(`${snakeCase(key).toUpperCase()} = ${buildOptionValue(options[key])}`));
    }
    return keys;
}
exports.buildOptions = buildOptions;
class CreateKeyspaceStatement extends BaseStatement_1.BaseStatement {
    as(...args) {
        if (args.length === 1 && args[0] instanceof Raw_1.Raw) {
            this.keyspaceDef = args[0];
            return this;
        }
        let definition = `"${args[0]}"`;
        this.keyspaceDef = new Raw_1.Raw(definition);
        return this;
    }
    withOptions(options) {
        this.optionsDef = options;
        return this;
    }
    createColumnFamily(name) {
        const statement = new CreateColumnFamilyStatement_1.CreateColumnFamilyStatement(this.builder);
        statement.keyspace(this.keyspaceDef);
        statement.as(name);
        return statement;
    }
    ifNotExists(value = true) {
        this.ifNotExistsDef = value;
        return this;
    }
    validate() {
        if (this.keyspaceDef == null) {
            throw new Error('The KEYSPACE name was not defined');
        }
        if (this.optionsDef == null || this.optionsDef.replication == null) {
            throw new Error('The REPLICATION options were not defined');
        }
    }
    getParams() {
        const params = [];
        params.push(...this.keyspaceDef.params);
        return params;
    }
    getQuery() {
        let query = `CREATE KEYSPACE ${this.ifNotExistsDef === true ? 'IF NOT EXISTS ' : ''}${this.keyspaceDef}`;
        const withDefs = [];
        if (this.optionsDef != null) {
            const options = Object.assign({}, this.optionsDef);
            if (options.replication != null) {
                const replication = Object.assign({}, options.replication);
                if (replication.class === 'NetworkTopologyStrategy') {
                    Object.assign(replication, replication.replicationFactor);
                    delete replication.replicationFactor;
                }
                options.replication = replication;
            }
            withDefs.push(...buildOptions(options));
        }
        if (withDefs.length) {
            query += ` WITH ${withDefs.join(' AND ')}`;
        }
        return query;
    }
    build() {
        this.validate();
        return new Raw_1.Raw(this.getQuery(), this.getParams());
    }
}
CreateKeyspaceStatement.CreateColumnFamily = CreateColumnFamilyStatement_1.CreateColumnFamilyStatement;
exports.CreateKeyspaceStatement = CreateKeyspaceStatement;
//# sourceMappingURL=CreateKeyspaceStatement.js.map