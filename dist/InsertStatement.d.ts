import { BaseStatement } from './BaseStatement';
import { Raw } from './Raw';
import { StringKey, TransformPossibleColumnDefinition } from './ColumnDefinition';
export declare class InsertStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends BaseStatement {
    protected columnDefs: Raw[];
    protected valueDefs: Raw[];
    protected keyspaceDef: Raw;
    protected columnFamilyDef: Raw;
    protected ttl: number;
    protected timestamp: number;
    keyspace(name: string | Raw): this;
    into(name: string | Raw): this;
    into(keyspace: string | Raw, name: string | Raw): this;
    values(data: Partial<{
        [K in StringKey<S>]: S[K];
    }>): this;
    value<K extends StringKey<S>>(column: K, value: S[K]): this;
    usingTTL(unix: number): this;
    usingTTL(date: Date): this;
    usingTTL(interval: string): this;
    usingTimestamp(unix: number): this;
    usingTimestamp(date: Date): this;
    usingTimestamp(interval: string): this;
    protected validate(): void;
    protected getParams(): any[];
    protected getQuery(): string;
    build(): Raw;
}
