"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
describe('Update statements', () => {
    const builder = new __1.Builder();
    it('Basic', () => {
        const { query, params } = builder
            .update('User')
            .set({
            'displayName': 'John Doe',
            'bio': '',
        })
            .add('groups', ['1'])
            .whereEquals('id', 'foo')
            .build();
        expect(query).toBe(`UPDATE "User" SET "displayName" = ?, "bio" = ?, "groups" = "groups" + ? WHERE "id" = ?`);
        expect(params.length).toBe(4);
        expect(params[0]).toBe('John Doe');
        expect(params[1]).toBe('');
        expect(params[2]).toEqual(['1']);
        expect(params[3]).toBe('foo');
    });
});
//# sourceMappingURL=UpdateStatement.test.js.map