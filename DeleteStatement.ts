import { WhereStatement } from './WhereStatement';
import { Raw } from './Raw';
import {
  StringKey,
  TransformPossibleColumnDefinition,
} from './ColumnDefinition';

export class DeleteStatement<S extends TransformPossibleColumnDefinition = TransformPossibleColumnDefinition> extends WhereStatement<S> {

  protected columnDefs: Raw[] = [];
  protected keyspaceDef: Raw;
  protected columnFamilyDef: Raw;

  columns<K extends StringKey<S>>(columns: K[]): this {
    for (const column of columns) {
      this.column(column);
    }
    return this;
  }

  column<K extends StringKey<S>>(name: K): this {
    this.columnDefs.push(new Raw(name));
    return this;
  }

  keyspace(name: string | Raw) {
    if (name instanceof Raw) {
      this.keyspaceDef = name;
    } else {
      this.keyspaceDef = new Raw(`"${name}"`);
    }
    return this;
  }

  from(name: string | Raw): this;
  from(keyspace: string | Raw, name: string | Raw): this;
  from(...args: any[]) {
    if (args.length === 1) {
      if (args[0] instanceof Raw) {
        this.columnFamilyDef = args[0];
      } else {
        this.columnFamilyDef = new Raw(`"${args[0]}"`);
      }
    } else {
      this.keyspace(args[0]);
      if (args[1] instanceof Raw) {
        this.columnFamilyDef = args[1];
      } else {
        this.columnFamilyDef = new Raw(`"${args[1]}"`);
      }
    }
    return this;
  }

  protected validate() {
    if (this.columnFamilyDef == null) {
      throw new Error(`No FROM column family was defined`);
    }
  }

  protected getParams() {
    const params: any[] = [];
    for (const def of this.columnDefs) {
      params.push(...def.params);
    }
    params.push(...this.columnFamilyDef.params);
    params.push(...this.getWhereParams());
    return params;
  }

  protected getQuery(): string {
    let query = `DELETE`;

    if (this.columnDefs.length) {
      query += ` ${this.columnDefs.join(', ')}`;
    }

    query += ` FROM ${typeof this.keyspaceDef !== 'undefined' ? `${this.keyspaceDef}.`: ''}${this.columnFamilyDef}`;

    query += this.getWhereQuery();

    return query;
  }

  build(): Raw {
    this.validate();
    return new Raw(this.getQuery(), this.getParams());
  }

}
